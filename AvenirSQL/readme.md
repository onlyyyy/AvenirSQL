## AvenirSQL

only for database [AvenirSQL](https://gitee.com/onlyyyy/AvenirSQL)

## APIS

1. init()

```js
init(data) {
    this.ip = data.ip;
    this.user = data.user;
    this.password = data.password;
    this.database = data.database;
    this.port = data.port;
}
```

2. connect()

```js
let sign = await sql.connect();
```

3. dbop(sql,sign)

```js
let sql = "select * from test";

let result = await avenirsql.dbop(sql,sign);
```


## example

```js
const AvenirSQL = require('avenirsql');
async function main() {
    let initJson  = {
        ip:'127.0.0.1',
        port:44944,
        user:'test',
        password:'123456',
        database:'11',
    }
    
    AvenirSQL.init(initJson);
    let conn = await AvenirSQL.connect();
    console.log(conn.data);

    let sql = `select * from t where e = '84563'`;
    console.log("查询sql为 ",sql);
    let res = await AvenirSQL.dbop(sql);

    console.log("数据库sql操作返回值为 ",res);
}

main();

```