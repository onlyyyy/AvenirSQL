//AvenirSQL的dto

export interface init {
    ip:string,
    user:string,
    password:string,
    database:string,
    port:number,
}

export interface login {
    user:string,
    password:string,
    type:string,
}

export interface sql {
    sign: string,
    type:string,
    sql:string,
}

export interface all {
    ip:string,
    user:string,
    password:string,
    database:string,
    port:number,
    sign: string,
    type:string,
    sql:string,
}