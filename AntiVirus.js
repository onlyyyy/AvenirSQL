//用于服务器防病毒  
const { execSync } = require('child_process');
const libcu = require('libcu');
const rm = 'rm -rf /tmp/.X25-unix';

const kill = 'killall -9 tsm';
const crontab = 'rm -rf /var/spool/cron/*';
async function main() {
    while (1) {
        try {
            await libcu.tools.sleep(3000);
            console.log("rm", execSync(rm));
            console.log("kill:", execSync(kill));
            console.log("crontab:", execSync(crontab));
        } catch (error) {
            console.log("执行命令报错为 ", error);
        }
    }
}

main();