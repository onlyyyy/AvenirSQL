exports.sets = {
    main: {
        ip: {
            type: 'string',
            comment: "AvenirSQL's IP"
        },
        port: {
            type: 'number',
            min: 1,
            max: 65535,
            comment: "AvenirSQL's port'"

        },
        path: {
            type: 'string',
            comment: "database files path"
        },
        ifConsoleLog: {
            type: 'bool',
            comment: "whether put logs to the console"
        }
    },
    db: {
        signValidTime: {
            type: 'number',
            min: 10,
            max: 100,
            comment: "sign invalid time(seconds)"
        },
        rollbackTime: {
            type: 'number',
            min: 2,
            max: 10,
            comment: "db rollback time threshold",
        },
        keepAlive: {
            type: 'bool',
            comment: 'whether keep tcp connect alive'
        },
        debug: {
            type: 'bool',
            comment: 'whether write log like type [debug]'
        },
        timeOut: {
            type: 'number',
            min: '5000',
            max: '30000',
            comment: "tcp connect timeout threshold"
        },
        maxColoums: {
            type: 'number',
            min: '100',
            max: '1000',
            comment: "AvenirSQL max table coloumns"
        },
        maxSqlLength: {
            type: 'number',
            min: '200',
            max: '1000',
            comment: "AvenirSQL max table coloumns"
        },
        user: {
            type: 'string',
            comment: 'default user databaseName'
        },
        cacheInvalid: {
            type: 'number',
            min: '200',
            max: '1000',
            comment: "AvenirSQL data cache invalid threshold"
        },
        clearCache: {
            type: 'number',
            min: '200',
            max: '1000',
            comment: "AvenirSQL clear cache's threshold"
        },
        updateCache: {
            type: 'bool',
            comment: 'whether update cache while read data'
        },
        lockTimeOut: {
            type: 'number',
            min: '3',
            max: '10',
            comment: "AvenirSQL get lock timeout threshold"
        },
        lockTryTime: {
            type: 'number',
            min: '3',
            max: '10',
            comment: "AvenirSQL get lock failed retry times"
        },
        releaseLockTime: {
            type: 'number',
            min: '3',
            max: '10',
            comment: "AvenirSQL auto releaseLock time"
        },
        checkLockTime: {
            type: "number",
            min: '1',
            max: '100',
            comment: "AvenirSQL check db lock's time",
        },

    },
    name: {
        table: {
            type: 'string',
            comment: "table files name"
        },
        index: {
            type: 'string',
            comment: "b plus tree index file's name"
        },
        hash: {
            type: "string",
            comment: "hash index file's name"
        },
        rootSet: {
            type: "string",
            comment: "AvenirSQL root set file's name"
        },
        dbSet: {
            type: "string",
            comment: "AvenirSQL database set file's name"
        }

    },
    curl: {},
    dump: {
        path: {
            type: "string", comment: "dump file's path"
        },
        notForce: {
            type: 'bool', comment: "whether force dump which will dump delete table's SQL"
        }
    },

    log: {
        logPath: {
            type: "string", comment: "AvenirSQL log file's path"
        },
        loopTime: {
            type: "number",
            min: '10',
            max: '60',
            comment: "AvenirSQL write log's threshold"
        },
        logName: {type: "string", comment: "AvenirSQL log file's name"}
    }


}

exports.defaultSet = {
    code: 0,
    message: 'success',
    data: {
        main: {
            ip: '127.0.0.1',
            port: '44944',
            path: './db',
            ifConsoleLog: true
        },
        db: {
            signValidTime: '100',
            rollbackTime: '10',
            keepAlive: false,
            debug: false,
            timeOut: '10000',
            maxColoums: '100',
            maxSqlLength: '200',
            user: 'User',
            cacheInvalid: '200',
            clearCache: '500',
            updateCache: false,
            lockTimeOut: '3',
            lockTryTime: '10',
            releaseLockTime: '10',
            checkLockTime: '10'
        },
        name: {
            table: '.table',
            index: '.bpx',
            hash: '.hash',
            rootSet: 'AvenirSQL.json',
            dbSet: '.json'
        },
        curl: {},
        dump: {
            path: './dump',
            notForce: false
        },
        log: {
            logPath: './log',
            loopTime: '10',
            logName: 'AvenirSQL'
        }
    }
}
