//本文件用于展示sqlParse的结果

//select * from test
let common_select = {
    "type": "select",
    "distinct": null,
    "columns": "*",
    "from": [
        {
            "db": "",
            "table": "test",
            "as": null
        }
    ],
    "where": null,
    "groupby": null,
    "orderby": null,
    "limit": null,
    "params": []
};


//create table test (a primary key not null)
let create_table = {

    type: 'create_table',
    name: { db: '', table: 'test' },
    columns: [{ name: 'a', type: [Object], ext: [Array] }],
    tableOptions: null,
    partitionOptions: null,
    params: []
}
columns = [
    {
        name: 'a',
        type: { type: 'varchar', args: [Array] },
        ext: ['primary', 'key', 'not', [Object]]
    }
]
astObj.columns.type = { type: 'varchar', args: ['10'] }
astObj.columns.ext = ['primary', 'key', 'not', { type: 'null', value: null }]


// insert into test.test (name,id) values (1,2)
let insert = {
    type: 'insert',
    db: 'test',
    table: 'test',
    columns: ['name', 'id'],
    values: [{ type: 'expr_list', value: [Array] }],
    params: []
}
columns = ['name', 'id'];
astObj.values[0].value = [{ type: 'string', value: '1' }, { type: 'string', value: '2' }];


// delete
let deleted = {
    type: 'delete',
    from: [{ db: '', table: 'test', as: null }],
    where: {
        type: 'binary_expr',
        operator: '=',
        left: { type: 'column_ref', table: '', column: 'a' },
        right: { type: 'string', value: '1' }
    },
    params: []
}

let delete_and = {
    type: 'delete',
    from: [{ db: '', table: 'test', as: null }],
    where: {
        type: 'binary_expr',
        operator: 'AND',
        left: {
            type: 'binary_expr',
            operator: '=',
            left: [Object],
            right: [Object]
        },
        right: {
            type: 'binary_expr',
            operator: '=',
            left: [Object],
            right: [Object]
        }
    },
    params: []
}

astObj.where.left = {
    type: 'binary_expr',
    operator: '=',
    left: { type: 'column_ref', table: '', column: 'a' },
    right: { type: 'string', value: '1' }
}


let delete_where_and_and = {
    type: 'delete',
    from: [{ db: '', table: 't', as: null }],
    where: {
        type: 'binary_expr',
        operator: 'AND',
        left: {
            type: 'binary_expr',
            operator: 'AND',
            left: [Object],
            right: [Object]
        },
        right: {
            type: 'binary_expr',
            operator: '=',
            left: [Object],
            right: [Object]
        }
    },
    params: []
}
columns = undefined
astObj.where.left = {
    type: 'binary_expr',
    operator: 'AND',
    left: {
        type: 'binary_expr',
        operator: 'AND',
        left: {
            type: 'binary_expr',
            operator: 'AND',
            left: [Object],
            right: [Object]
        },
        right: {
            type: 'binary_expr',
            operator: '=',
            left: [Object],
            right: [Object]
        }
    },
    right: {
        type: 'binary_expr',
        operator: '=',
        left: { type: 'column_ref', table: '', column: 'd' },
        right: { type: 'string', value: '4' }
    }
}