//每一列数据的分隔符
exports.WHITE_SPACE = "∫";

exports.errCode = {
    //不该发生的错误
    UNKNOWN_CMD: {
        code: -1,
        message: 'unknown command',
    },
    SYSTEM_BUSY: {
        code: -2,
        message: 'system busy',
    },
    BAD_REQUEST: {
        code: -3,
        message: 'bad request',     //请求格式无法JSON序列化
    },
    UNKNOWN_ERROR: {
        code: -100,
        message: 'unknown error'
    },
    //成功
    AVENIR_SUCCESS: {
        code: 0,
        message: 'success'
    },
    //程序级别错误
    SQL_PARSE_ERROR: {
        code: 1,
        message: 'sql parse error'
    },
    NOT_CONNECTED: {
        code: 2,
        message: "no connect info, please login first"
    },
    //大意了没有3 4不吉利 年轻人不讲5的
    FILE_NOT_EXIST: {
        code: 6,
        message: 'file not exist'
    },
    GEN_SIGN_ERROR: {
        code: 7,
        message: 'generate sign error',
    },
    SET_SIGN_EXIT: {
        code: 8,
        message: 'sign exit',
    },
    LACK_OF_SIGN: {
        code: 9,
        message: 'lack of sign, please login first',
    },
    INVALID_NAME: {
        code: 10,
        message: "invalid name",
    },
    BAD_USER: {
        code: 11,
        message: 'user or password error',
    },
    PERMISSION_DENIED: {
        code: 12,
        message: 'permission denied',
    },


    //以下是数据库类错误
    DATABASE_NOT_FOUND: {
        code: 1001,
        message: 'database not found'
    },
    DATABASE_EXIST: {
        code: 1002,
        message: 'database already exist',
    },
    TABLE_NOT_FOUND: {
        code: 1003,
        message: 'table not found'
    },
    TABLE_EXIST: {
        code: 1004,
        message: 'table already exist'
    },
    INVALID_SQL_ERROR: {
        code: 1005,
        message: "invalid sql or sql parse Error"
    },
    TOO_MANY_COLUMNS: {
        code: 1006,
        message: 'too many columns',
    },
    COLUMN_NOT_FOUND: {
        code: 1007,
        message: 'column not found',
    },
    COLUMN_NOT_MATCH: {
        code: 1008,
        message: 'columns and values not match',
    },
    COLUMN_REPEAT: {
        code: 1009,
        message: 'columns repeat',
    },
    COLUMN_NOT_NULL: {
        code: 1010,
        message: 'some columns cant be null',
    },
    COLUMN_NOT_CHECK: {
        code: 1011,
        message: 'column not check error',
    },
    ONLY_ONE_KEY: {
        code: 1012,
        message: 'AvenirSQL only support one key',
    },
    LACK_OF_PRIMARY_KEY: {
        code: 1013,
        message: 'lack of primary key',
    },
    KEY_EXIST: {
        code: 1014,
        message: 'duplicate primary key value',
    },
    OPER_NO_ROW: {
        code: 1015,
        message: 'the operated row not found, may be not a error'
    },
    VALUE_NOT_NUMBER: {
        code: 1016,
        message: 'compared value is not a number',
    },
    SQL_TOO_LONG: {
        code: 1017,
        message: 'sql is too long',
    },
    SQL_NOT_SUPPORT: {
        code: 1018,
        message: 'AvenirSQL dont support this sql yet',
    },
    GET_LOCK_FAILED: {
        code: 1019,
        message: 'AvenirSQL get lock timeout',
    },
    RELEASE_LOCK_FAILED: {
        code: 1020,
        message: 'AvenirSQL release lock failed',
    },
    USER_EXISTS: {
        code: 1021,
        message: 'user already exists',
    },
    USER_NOT_FOUND: {
        code: 1022,
        message: 'bad user or password',
    },
    //事务类错误
    TRANS_NOT_FOUND: {
        code: 1023,
        message: 'invalid trans id',
    },
    TRANS_TIME_OUT: {
        code: 1024,
        message: 'trans timeout',
    },
    NO_GROUP_DIS: {
        code: 1025,
        message: 'AvenirSQL dont support group by yet'
    },
    COUNT_NO_TABLE: {
        code: 1026,
        message: `count(colums) cant be [table.column]`
    },
    WHITE_SPACE_ERROR: {
        code: 1027,
        message: `sql cant contain AvenirSQL's separator:${this.WHITE_SPACE}`,
    },
    NOT_SUPPORT_DATA: {
        code: 1028,
        message: 'AvenirSQL dont support such data type',
    },
    COLUMN_TYPE_ERROR: {
        code: 1029,
        message: 'table columns type error(number or string)',
    },
    COLUMN_OUT_OF_LENGTH: {
        code: 1030,
        message: 'columns over the length',
    },
    DUMP_TABLENAME_ERROR: {
        code: 1031,
        message: 'dumped table name error'
    },
    DUMP_SQL_ERROR: {
        code: 1032,
        message: "dump sql error"
    },
    SQLFILE_NOT_FOUND:{
        code:1033,
        message:'sql file not found',
    },
    SET_SQL_ERROR:{
        code:1034,
        message:'set type error,use set or set [main] [sub] [value]'
    },
    MAIN_NAME_ERROR:{
        code:1035,
        messsage:'sets main key not found',
    },
    SUB_NAME_ERROR:{
        code:1036,
        message:'sets sub key not found'
    },
    SET_TYPE_UNKNOWN:{
        code:1037,
        message:'unknown set type ,support number/string/bool '
    },
    SET_TYPE_ERROR:{
        code:1038,
        message:"set type error",
    },
    SET_VALUE_OUT:{
        code:1039,
        message:"set value out of threshold"
    },
    SET_VALUE_SAME:{
        code:1040,
        message:'set value is equal,nothing to do'
    }


}


//是否单人开发
exports.ifSingle = false;

exports.AES_KEY = "AvenirSQL";  //弃用，必须是16位

exports.NULL_LINE = 'AVENIR_NULL';        //代表数据表的某一行暂时是空行

// 基本只有目前的两处要用
exports.unknown = {
    code: -100,
    message: 'unknown error',
}


//成功了就抛 {code : SUCCESS , data : xxx}
exports.SUCCESS = 'AVENIR_SUCCESS';

//只有root才能操作的数据库
exports.roots = {
    User: true
}


//定义AvenirSQL的基本数据类型 先判断是否有这个对象  再判断是否是数字
//int varchar实际上是为了兼容其他数据库的使用习惯 len为false代表不判断长度
exports.dataTypes = {
    number: {
        isNumber: true,
        len: 10
    },
    string: {
        isNumber: false,
        len: 20
    },
    int: {
        isNumber: true,
        len: 10,
    },
    varchar: {
        isNumber: false,
        len: 20,
    },
    bignumber: {
        isNumber: true,
        len: false,
    },
    bigstring: {
        isNumber: false,
        len: false,
    }
}
