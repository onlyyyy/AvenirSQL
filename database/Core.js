// AvenirSQL核心模块
const fs = require('fs');
const nthfile = require('nth-file');
const path = require('path');
const INI = require('ini');
const {SUCCESS, roots, WHITE_SPACE, dataTypes} = require('./common/constants');
const {
    readIndex,
    countLine,
    EOL,
    delQuotation,
    toLog,
    ifObjectIsEmpty,
    ifBpt
} = require('./common/common');
const Btree = require('sorted-btree').default;
const libcu = require('libcu');
const del = require('del');
const MultipleTree = require('multiple-tree');
const set = require('./common/set');

class Core {
    constructor() {

    }

    //判断该用户是否有权限
    checkAuth(database, sign) {
        if (roots[database]) {
            if (!sign || !conn.isRoot(sign)) {
                throw ('PERMISSION_DENIED');
            }
        }
        return true;
    }

    //获取数据库设置文件
    async getSet(notNodis) {
        let set = notNodis ? null : await nodis.get("dbSet", null);
        toLog("Nodis数据库配置的缓存为 ", set);
        if (set) {
            toLog("从缓存中获取dbSet");
            return set;
        }
        let setPath = ini.name.rootSet;
        set = fs.readFileSync(path.join(ini.main.path, setPath));
        set = JSON.parse(set);
        await nodis.put("dbSet", null, set);
        return set;
    }

    //写数据库配置文件
    async writeSet(data) {
        let setPath = path.join(ini.main.path, ini.name.rootSet);
        fs.writeFileSync(setPath, JSON.stringify(data));
        await nodis.put('dbSet', null, data);
    }

    async getTableDefine(tableName, tableFile, dbName, notNodis) {
        let res = notNodis ? null : await nodis.get('tableDefine', tableName, dbName);
        toLog("Nodis 关于表" + tableName + "的缓存为", res);
        if (!res) {
            res = await readIndex(0, tableFile);
            res = JSON.parse(res.toString());
            await nodis.put('tableDefine', tableName, res, dbName);
        }
        return res;
    }

    async getHashIndex(tableName, hashFile, dbName, notNodis) {
        let res = notNodis ? null : await nodis.get('hash', tableName, dbName);
        toLog("Nodis 关于表" + tableName + "的hash索引缓存为", res);
        if (!res) {
            res = fs.readFileSync(hashFile);
            res = JSON.parse(res);
            await nodis.put('hash', tableName, res, dbName);
        }
        return res;
    }

    async getFileArrCache(tableName, tableFile, dbName, notNodis) {
        let res = notNodis ? null : await nodis.get('tableData', tableName, dbName);
        toLog("Nodis 关于表" + tableName + "的行数据缓存为", res);
        if (!res) {
            res = nthfile.getFileArr(tableFile);
            // //掐头去尾
            // res.splice(0, 1);
            // res.splice(res.length - 1, 1);
            await nodis.put('tableData', tableName, res, dbName);
        }
        return res;
    }

    //传表结构获取其中的主键和列数量
    async getTableKeyAndNum(tableDefine, tableName, dbName, notNodis) {
        let res = notNodis ? null : await nodis.get('tableDetail', tableName, dbName);
        toLog("Nodis 关于表" + tableName + "的主键和列缓存为", res);
        if (!res) {
            let key = null;
            let columns = tableDefine.table;
            let num = 0;
            for (let k in columns) {
                if (columns[k].key) {
                    key = k;
                }
                num++;
            }
            res = {
                key,
                num,
            };
            await nodis.put('tableDetail', tableName, res, dbName);

        }
        return res;
    }

    async getBPTreeFromCache(bptFile, tableName, dbName, notNodis) {
        let res = notNodis ? null : await nodis.get('bpTree', tableName, dbName);
        toLog("Nodis 关于表" + tableName + "的B+树索引缓存为", res);
        if (!res) {
            res = fs.readFileSync(bptFile);
            res = JSON.parse(res);
            await nodis.put('bpTree', tableName, res, dbName);
        }
        return res;
    }


    //传数据库名字 获取数据库和表文件名
    getFileNames(database, tableName, haveTable) {
        let dbPath = path.join(ini.main.path, database);
        if (!fs.existsSync(dbPath)) {
            throw ('DATABASE_NOT_FOUND');
        }
        let tableFile = path.join(dbPath, tableName + ini.name.table);
        //有传haveTable就代表期望表是存在的，否则期望表不存在
        if (haveTable) {
            if (!fs.existsSync(tableFile)) {
                throw ('TABLE_NOT_FOUND');
            }
        } else {
            if (fs.existsSync(tableFile)) {
                throw ('TABLE_EXIST');
            }
        }

        let hashFile = path.join(dbPath, tableName + ini.name.hash);
        let bptFile = path.join(dbPath, tableName + ini.name.index);
        return {
            dbPath,
            tableFile,
            hashFile,
            bptFile,
        }
    }


    //处理set命令 修改AvenirSQL的设置
    async doSet(main, sub, value) {
        let sets = set.sets;
        if (!sets[main]) {
            throw('MAIN_NAME_ERROR');
        }

        //没有value就是只吐出配置而不进行修改
        if(!value) {
            if(main) {
                if(sub) {
                    throw({
                        code:SUCCESS,
                        data:sets[main][sub]
                    })
                } else {
                    //将所有的节点都输出
                    throw{
                        code:SUCCESS,
                        data:sets[main]
                    }
                }
            } else {
                throw("MAIN_NAME_ERROR")
            }
        } else {
            //找不到子节点
            if(!sets[main][sub]) {
                throw('SUB_NAME_ERROR');
            }
            switch (sets[main][sub].type) {
                case 'number':
                    //必然有max min
                    if(!_.isNumber(parseFloat(value))) {
                        throw('SET_TYPE_ERROR');
                    }
                    if(_.toNumber(value) > sets[main][sub].max || _.toNumber(value) < sets[main][sub].min) {
                        throw("SET_VALUE_OUT")
                    }
                    //不同类型的值做一些细微处理
                    if(ini[main][sub] === parseFloat((value))) {
                        throw('SET_VALUE_SAME');
                    }
                    ini[main][sub] = parseFloat((value));
                    break;
                case 'string':
                    if(!_.isString(value) || !_.isNaN(parseInt(value))) {
                        throw('SET_TYPE_ERROR');
                    }
                    if(ini[main][sub] === value) {
                        throw("SET_VALUE_SAME");
                    }
                    ini[main][sub] = value;
                    break;
                case 'bool':
                    if(value!== "true" && value !== "false" ) {
                        throw('SET_TYPE_ERROR');
                    }
                    ini[main][sub] = value === "true";
                    break;
                default :
                    throw('SET_TYPE_UNKNOWN');
            }

            //写文件
            fs.writeFileSync(define.fileName,INI.stringify(ini));
            throw(SUCCESS);
        }

    }

    //创建数据库 暂时不考虑存储页等问题
    async createDb(dbName) {
        if (!dbName) {
            throw ('INVALID_NAME');
        }
        let dbPath = ini.main.path;

        let dir = path.join(dbPath, dbName);
        if (fs.existsSync(dir)) {
            throw ('DATABASE_EXIST');
        } else {
            fs.mkdirSync(dir, {recursive: true});        //这样会递归创建目录
            this.initDb(dbName);
            dbName === 'User' || await this.changeDefaultDb(dbName);
            await nodis.clearTableCache(['dbSet'], null, dbName); //tableDefine可以不清除
            if (ini.db.updateCache === true) {
                await this.refreshCache(['dbSet'], dbName);
            }
            throw (SUCCESS);
        }
    }

    //初始化创建数据库设置文件 注意这里的dbName已经是全路径
    initDb(dbName) {
        let fr = ini.main.path;

        let fileName = path.join(path.join(fr, dbName), dbName + ini.name.dbSet);
        fs.writeFileSync(fileName, '{}');
    }

    //修改默认数据库
    async changeDefaultDb(dbName) {
        let fileName = path.join(ini.main.path, ini.name.rootSet);
        if (!fs.existsSync(fileName)) {
            throw ('FILE_NOT_EXIST');
        }
        let data = JSON.parse(fs.readFileSync(fileName).toString());
        data.defaultDb = dbName;
        fs.writeFileSync(fileName, JSON.stringify(data));
        await nodis.clearTableCache(['dbSet'], null, dbName); //tableDefine可以不清除
        if (ini.db.updateCache === true) {
            await this.refreshCache(['dbSet'], dbName);
        }
    }


    //切换数据库 改写ini.name.rootSet文件下的defaultDB字段
    async useDB(dbName) {
        toLog("选择切换数据库至 ", dbName);
        if (!dbName) {
            throw ('INVALID_NAME');
        }
        await this.changeDefaultDb(dbName);
        throw (SUCCESS);
    }

    // 异步建表 以后可以加锁
    // 20201209 原则上sql解析器没有报错的sql，就尽可能完成建表操作
    async createTable(sql, sign) {

        //读数据库配置文件 选择数据库 数据库配置文件不可能是空的
        let set = await this.getSet();
        toLog('数据库配置文件为 ', set);
        let database = sql.name.db || set.defaultDb;
        if (!database) {
            throw ('DATABASE_NOT_FOUND');
        }
        this.checkAuth(database, sign);
        let tableName = sql.name.table;
        let tableOptions = sql.tableOptions;
        let columns = sql.columns;  // array

        //判断列是否超出上限
        if (columns.length > ini.db.maxcolumns) {
            throw ('TOO_MANY_COLUMNS');
        }

        let partitionOptions = sql.partitionOptions;
        let params = sql.params;    // array
        let paths = this.getFileNames(database, tableName, false);
        let table = paths.tableFile;
        let index = paths.bptFile;
        let hash = paths.hashFile;


        let first = {
            tableOptions,
            partitionOptions,
            params,
        };
        let t = {};
        let keyText = null;
        let ifKey = false;
        let whatskey = null;
        //解析建表语句中的主键和not语句
        for (let i = 0; i < columns.length; i++) {
            t[columns[i].name] = {};
            if (!dataTypes[columns[i].type.type]) {
                throw ('NOT_SUPPORT_DATA');
            }
            t[columns[i].name].type = columns[i].type.type;
            t[columns[i].name].len = columns[i].type.args ? columns[i].type.args[0] : '';
            let key = false, primary = false, not = false, notObj = null, index = false;
            for (let j = 0; j < columns[i].ext.length; j++) {
                if (columns[i].ext[j] === 'key') {
                    whatskey = columns[i].ext[j].value;
                    key = true;
                } else if (columns[i].ext[j] === 'primary') {
                    primary = true;
                } else if (columns[i].ext[j] === 'not') {
                    not = true;
                } else if (typeof columns[i].ext[j] == 'object') {
                    notObj = columns[i].ext[j].type === 'null' ? 'null' :
                        columns[i].ext[j].value;
                } else if (columns[i].ext[j] === 'index') {
                    index = true;
                }
            }
            t[columns[i].name].locate = i;
            t[columns[i].name].key = key && primary;
            // 判断是否有重复的主键
            if (key && primary) {
                keyText = columns[i].name;
                if (!ifKey) {
                    ifKey = true;
                } else {
                    throw ('ONLY_ONE_KEY');
                }
            }
            t[columns[i].name].not = (not && notObj) ? notObj : null;
            t[columns[i].name].index = index;
        }
        //初始化B+树 生成一颗空树
        fs.writeFileSync(index, JSON.stringify(this.initBpTree(whatskey)));

        // 判断是否没有主键
        if (!ifKey) {
            throw ('LACK_OF_PRIMARY_KEY');
        }

        first.table = t;
        let hashText = {};
        hashText[keyText] = {};
        hashText = JSON.stringify(hashText);
        fs.writeFileSync(hash, hashText);

        first = JSON.stringify(first);
        first += EOL;
        toLog("表结构为 ", first);
        fs.writeFileSync(table, first);
        toLog("建表成功");
        throw (SUCCESS);
    }

    //删除表功能
    async dropTable(tableName, sign) {
        let array = tableName.split('.');
        let db = null;

        if (array.length === 1) {
            tableName = array[0];
        } else if (array.length === 2) {
            tableName = array[1];
            db = array[0];
        } else {
            throw ('SQL_PARSE_ERROR');
        }

        toLog("需要删除的表为 ", tableName);
        let set = await this.getSet();
        toLog('数据库配置文件为 ', set);
        let database = db || set.defaultDb;
        let dbPath = path.join(ini.main.path, database);
        if (!fs.existsSync(dbPath)) {
            throw ('DATABASE_NOT_FOUND');
        }
        let paths = this.getFileNames(database, tableName, true);

        let hashFile = paths.hashFile;
        let bptFile = paths.bptFile;
        let tableFile = paths.tableFile;
        if (!fs.existsSync(hashFile) || !fs.existsSync(bptFile) || !fs.existsSync(tableFile)) {
            throw ("TABLE_NOT_FOUND");
        }
        //把要删除的内容丢数组里
        let deletes = [hashFile, bptFile, tableFile];
        toLog("deletes = ", deletes);
        await lock.getLock(database, tableName, 0, sign, null, null);

        try {
            let files = await libcu.cf.walkFolder(dbPath);
            for (let i = 0; i < files.fileList.length; i++) {
                //判断是否在要删除的数组内
                if (libcu.tools.contains(deletes, files.fileList[i])) {
                    if (fs.existsSync(files.fileList[i])) {
                        //删除文件用unlinkSync rmSync会报找不到这个函数
                        fs.unlinkSync(files.fileList[i], {force: true});
                        toLog("删除了 ", files.fileList[i]);
                    } else {
                        throw ('TABLE_NOT_FOUND');
                    }
                }
            }
            throw (SUCCESS);
        } catch (error) {
            await lock.getLock(database, tableName, 1, sign, null, null);
            throw (error);
        }
    }

    //20201224 删库功能
    async dropDatabase(database) {
        toLog("需要删除的数据库为 ", database);
        let dbPath = path.join(ini.main.path, database);
        if (!fs.existsSync(dbPath)) {
            throw ('DATABASE_NOT_FOUND');
        }
        del.sync([dbPath]);
        //遍历目录 赋值一个默认的数据库
        let walks = await libcu.cf.walkFolder(ini.main.path);
        let dbs = walks.dirList;
        let set = await this.getSet();
        set.defaultDb = '';
        for (let i = 0; i < dbs.length; i++) {
            let dbName = path.basename(dbs[i]);
            if (dbName !== ini.db.user) {
                set.defaultDb = dbName;
                break;
            }
        }
        await this.writeSet(set);
        throw (SUCCESS);
    }

    //插入表
    async insert(sql, sign, flag, isTrans, transInfo) {
        //20210104 isTrans为true的时候，所有操作都在transInfo里进行处理
        let set = await this.getSet();
        toLog('数据库配置文件为 ', set);
        let database = sql.db || set.defaultDb;
        let table = sql.columns;              //插入语句的表结构
        let values = sql.values[0].value;            //插入语句的数据
        let tableName = sql.table;
        //列和数据不匹配
        if (table.length != values.length) {
            throw ('COLUMN_NOT_MATCH')
        }
        //列重复
        if (_.uniq(table).length !== table.length) {
            throw ('COLUMN_REPEAT');
        }
        let paths = this.getFileNames(database, tableName, true);

        let hashFile = paths.hashFile;
        let bptFile = paths.bptFile;
        let tableFile = paths.tableFile;

        //20210106 判断是否是第一次进行事务 不是的话就不等待锁了
        let firstTrans = false;
        if (isTrans && (!transInfo[database] || !transInfo[database][tableName])) {
            toLog(`初次执行事务 ,db = ${database} table = ${tableName}`);
            transInfo[database] = {};
            transInfo[database][tableName] = {
                tableData: null,
                tableDefine: null,
                hashIndex: null,
                bptFile: null,
                tableDetail: null,
                fileName: {
                    hashFile,
                    bptFile,
                    tableFile,
                }
            }
            firstTrans = true
        }
        let tableDefine = null;
        let tableDetail = null;

        if (isTrans) {
            tableDefine = transInfo[database][tableName].tableDefine || await this.getTableDefine(tableName, tableFile, database);
            tableDetail = transInfo[database][tableName].tableDetail || await this.getTableKeyAndNum(tableDefine, tableName, database);
            transInfo[database][tableName].tableData = await this.getFileArrCache(tableName, tableFile, database);
            transInfo[database][tableName].tableDefine = tableDefine;
            transInfo[database][tableName].tableDetail = tableDetail;
        } else {
            tableDefine = await this.getTableDefine(tableName, tableFile, database);
            tableDetail = await this.getTableKeyAndNum(tableDefine, tableName, database);
        }

        toLog("tableDefine = ", tableDefine);
        //表的列定义
        let tablecolumn = tableDefine.table;

        //生成一个要写入数据库的对象
        let data = {};
        //主键
        let tableKey = null;
        for (let key in tablecolumn) {
            if (tablecolumn[key].key === true) {
                tableKey = key;
            }
            data[key] = {
                value: null,
            }
        }
        let columns = sql.columns;
        for (let i = 0; i < columns.length; i++) {
            //注意这两个都是对象 只不过用了数据下标的调用方式
            if (!tablecolumn[columns[i]] || !data[columns[i]]) {
                toLog("!tablecolumn[columns[i]] || !data[columns[i]]");
                throw ('COLUMN_NOT_FOUND');
            } else {
                //这里不需要判断列是否存在了
                //20210311 如果是数字就转数字 是字符串不需要改动 为了配合列类型判断的新功能
                if (values[i].type === 'number') {
                    data[columns[i]].value = parseFloat(values[i].value);
                } else {
                    data[columns[i]].value = values[i].value;
                }
            }
        }
        // toLog("预定义的入库数据为 ", data);
        let keyValue = data[tableKey].value;
        (isTrans && !firstTrans) || await lock.getLock(database, tableName, 0, sign, null, null);
        try {
            let {dataStr} = await this.createLine(tableDefine, data);

            let hashIndex;
            let btree;
            if (isTrans) {
                [hashIndex, btree] = await Promise.all([
                    transInfo[database][tableName].hashIndex || this.getHashIndex(tableName, hashFile, database),
                    transInfo[database][tableName].bptFile || this.restoreBpTree(bptFile, tableName, database, tableDetail)
                ])

                transInfo[database][tableName].hashIndex = hashIndex;
                transInfo[database][tableName].bptFile = btree;
            } else {
                [hashIndex, btree] = await Promise.all(
                    [this.getHashIndex(tableName, hashFile, database),
                        this.restoreBpTree(bptFile, tableName, database, tableDetail)]
                )
            }
            await this.hashLine(tableFile, 'insert', tableKey, keyValue, hashFile, hashIndex, isTrans);

            //20210105 解决事务插入数据未更新tableData的bug
            !isTrans ? fs.appendFileSync(tableFile, dataStr) : transInfo[database][tableName].tableData.push(dataStr.replace(EOL, ''));


            let simpleLine = this.columnParseSimple(tableDefine, dataStr, tableDetail.num);
            simpleLine[tableDetail.key] = delQuotation(simpleLine[tableDetail.key]);
            let bptKey = {
                [tableDetail.key]: simpleLine[tableDetail.key],
            }
            btree.set(bptKey, simpleLine);
            !isTrans ? fs.writeFileSync(bptFile, JSON.stringify(btree)) : (transInfo[database][tableName].bptFile = btree);
            btree = undefined;  //gc

            //处理完之后再更新B+树索引      可以考虑从缓存读取然后更新数据 后期再更新
            await nodis.clearTableCache(['tableData', 'hash', 'bpTree'], tableName, database); //tableDefine可以不清除
            if (!isTrans && (ini.db.updateCache === true)) {
                await this.refreshCache(['tableData', 'hash', 'bpTree'], database, tableName, tableFile, hashFile, bptFile);
            }
            throw (SUCCESS);
        } catch (error) {
            toLog(error);
            //20210104事务中不解锁
            isTrans || await lock.getLock(database, tableName, 1, sign, null, null);
            if (isTrans) {
                throw ({code: error, trans: transInfo});
            }
            throw (error);
        }

    }


    //生成哈希索引 告诉数据库数据该如何存储
    async hashLine(tableFile, type, key, value, hashFile, hashIndex, isTrans) {

        if (!hashIndex[key]) {
            hashIndex[key] = {};
        }

        if (hashIndex[key][value]) {
            throw ('KEY_EXIST');
        }

        if (type === 'insert') {
            let count = countLine(tableFile);
            let temp = hashIndex[key];
            temp[value] = count;

        }
        if (isTrans) {
            return hashIndex;
        } else {
            fs.writeFileSync(hashFile, JSON.stringify(hashIndex));
        }
    }

    //传入key 生成一个B+树结构
    initBpTree(key) {
        let btree = new Btree(undefined, (a, b) => {
            if (a[key] > b[key]) {
                return 1;
            } else if (a[key] < b[key]) {
                return -1;
            } else {
                return a[key] - b[key];
            }
        });
        //20201227 这里初始化B+树拿到的数据居然是上一次生成的，what the fuck man
        btree._root.keys = [];
        btree._root.values = [];
        btree._root.isShared = true;
        btree._size = 0;
        btree._maxNodeSize = 32;

        return btree;
    }

    //从头创建B+树索引 传入表名 B+树文件名 生成B+树数据结构 写入文件
    async createBpTree(bptFile, tableFile, tableName, dbName, dontWrite) {
        if (!ifBpt) {
            return;
        }
        let [tableDefine, tableData] = await Promise.all([
            this.getTableDefine(tableName, tableFile, dbName),
            this.getFileArrCache(tableName, tableFile, dbName)
        ]);

        toLog("createBpTree的表结构信息为 ", tableData);
        let tableDetail = await this.getTableKeyAndNum(tableDefine, tableName, dbName);
        let key = tableDetail.key;
        //先初始化
        let btree = this.initBpTree(key);
        for (let i = 1; i < tableData.length - 1; i++) {
            let data = this.columnParseSimple(tableDefine, tableData[i], tableDetail.num);
            data[key] = delQuotation(data[key]);
            let bptKey = {
                key: data[key],
            }
            btree.set(bptKey, data);
        }
        if (dontWrite) {
            return btree;
        } else {
            fs.writeFileSync(bptFile, JSON.stringify(btree));
            btree = undefined;
        }


    }

    // 删除表
    async delete(sql, sign, flag, isTrans, transInfo) {
        toLog('sql = ', sql);
        toLog('sql.from = ', sql.from);
        let set = await this.getSet();
        //先解析目标数据表
        let database = sql.from[0].db;
        let tableName = sql.from[0].table;
        let asText = sql.as;

        database || (database = set.defaultDb);
        let paths = this.getFileNames(database, tableName, true);
        this.checkAuth(database, sign);
        let tableFile = paths.tableFile;
        let hashFile = paths.hashFile;
        let bptFile = paths.bptFile;
        let firstTrans = false;
        if (isTrans && (!transInfo[database] || !transInfo[database][tableName])) {
            toLog(`初次执行事务 ,db = ${database} table = ${tableName}`);
            transInfo[database] = {};
            transInfo[database][tableName] = {
                tableData: null,
                tableDefine: null,
                hashIndex: null,
                bptFile: null,
                tableDetail: null,
                fileName: {
                    hashFile,
                    bptFile,
                    tableFile,
                }
            }
            firstTrans = true;
        }

        (isTrans && !firstTrans) || await lock.getLock(database, tableName, 0, sign, null, null);
        try {

            let tableDefine = null;
            if (isTrans) {
                tableDefine = transInfo[database][tableName].tableDefine || await this.getTableDefine(tableName, tableFile, database);
                transInfo[database][tableName].tableDefine = tableDefine;
            } else {
                tableDefine = await this.getTableDefine(tableName, tableFile, database);
            }

            let tablecolumn = tableDefine.table;
            let where = {};
            //有内容再解析
            if (sql.where) {
                where = await this.parseWhere({}, sql.where);
            }
            toLog("where = ", JSON.stringify(where));
            if (JSON.stringify(where) === '{}') {
                toLog("清全表");

                let first = await readIndex(0, tableFile);
                first += EOL;
                if (isTrans) {
                    transInfo[database][tableName].hashIndex = '{}' + EOL;
                    transInfo[database][tableName].tableData = first;
                } else {
                    fs.writeFileSync(tableFile, first);
                    fs.writeFileSync(hashFile, '{}' + EOL);
                }

                //不用写B+树文件 后面通过函数自动生成了
            } else {
                //注意只有不是清全表才能拿左右子句  否则直接报错
                toLog("清某部分数据");
                //组合sql语句 多个column
                let deleteColumn = where;
                let tableDetail = null;
                if (isTrans) {
                    tableDetail = transInfo[database][tableName].tableDetail || await this.getTableKeyAndNum(tableDefine, tableName, database);
                    transInfo[database][tableName].tableDetail = tableDetail;
                } else {
                    tableDetail = await this.getTableKeyAndNum(tableDefine, tableName, database);
                }

                let key = tableDetail.key;
                let num = tableDetail.num;
                let hasKey = false;         //这次的操作是否有主键
                let AND = where['AND'];
                let OR = where['OR'];
                //先判断是不是有不存在的列
                for (let key in AND) {
                    if (!tablecolumn[key]) {
                        throw ('COLUMN_NOT_FOUND');
                    }
                    //判断是否有主键
                    if (tablecolumn[key].key) {
                        hasKey = true;
                    }
                }
                //OR不判断主键
                for (let key in OR) {
                    if (!tablecolumn[key]) {
                        throw ('COLUMN_NOT_FOUND');
                    }
                }


                toLog('hasKey = ', hasKey);
                if ((AND[key].operator == '=') && !OR) {
                    //走哈希索引 因为命中了主键
                    toLog("通过哈希索引来删除表");
                    let hashIndex = null;
                    let tableData = null;
                    if (isTrans) {
                        [hashIndex, tableData] = await Promise.all([
                            transInfo[database][tableName].hashIndex || this.getHashIndex(tableName, hashFile, database),
                            transInfo[database][tableName].tableData || this.getFileArrCache(tableName, tableFile, database)
                        ])
                        transInfo[database][tableName].hashIndex = hashIndex;
                        transInfo[database][tableName].tableData = tableData;

                    } else {
                        [hashIndex, tableData] = await Promise.all([
                            this.getHashIndex(tableName, hashFile, database),
                            this.getFileArrCache(tableName, tableFile, database)
                        ])
                    }

                    let getIndex = AND[key].value;
                    toLog('hashIndex[key] = ', hashIndex[key]);
                    toLog("hashIndex = ", hashIndex);
                    //20201227 先判断这张表是否为空 增加!hashIndex[key]的判断
                    if (!hashIndex[key] || !hashIndex[key][getIndex]) {
                        //没有这个元素
                        toLog("主键未命中");
                        throw (SUCCESS);
                    }
                    toLog('hashIndex[key][getIndex] = ', hashIndex[key][getIndex]);
                    let line = nthfile.read(tableFile, hashIndex[key][getIndex]);
                    let res = await this.checkColumnAndData(line, deleteColumn, tableDefine, num);
                    toLog('判断的结果为 ', res);
                    if (res) {
                        //写文件 改库
                        nthfile.delete(tableFile, hashIndex[key][getIndex]);
                        delete hashIndex[key][getIndex];
                        if (isTrans) {
                            //事务中直接对表缓存数组进行操作即可
                            transInfo[database][tableName].hashIndex = hashIndex;
                            transInfo[database][tableName].tableData = transInfo[database][tableName].tableData.splice(hashIndex[key][getIndex], 1);
                        } else {
                            fs.writeFileSync(hashFile, JSON.stringify(hashIndex));
                        }

                    } else {
                        throw (SUCCESS);
                    }
                } else {
                    //主键是范围查找
                    toLog('全表扫描来删除表');
                    let fileArr = null;
                    if (isTrans) {
                        fileArr = transInfo[database][tableName].tableData || await this.getFileArrCache(tableName, tableFile, database);
                        transInfo[database][tableName].tableData = fileArr;

                    } else {
                        fileArr = await this.getFileArrCache(tableName, tableFile, database);
                    }
                    let deletes = [];
                    for (let i = 0; i < fileArr.length; i++) {
                        if (!fileArr[i]) {
                            break;
                        }
                        let judge = await this.checkColumnAndData(fileArr[i], deleteColumn, tableDefine, num);
                        toLog('判断这行数据是否要被删除的结果 为 ', judge);
                        if (judge) {
                            deletes.push(i);
                        }

                    }
                    toLog('deletes = ', deletes);
                    if (deletes.length > 0) {
                        if (isTrans) {
                            let temp = transInfo[database][tableName].tableData;
                            let change = 0;
                            for (let i = 0; i < temp.length; i++) {
                                deletes[i] -= change;
                                if (deletes[i] < 0 || deletes[i] > temp.length) {
                                    throw ('index error')
                                }
                                temp.splice(deletes[i], 1);
                                change++;
                            }
                            transInfo[database][tableName].tableData = temp;
                        } else {
                            nthfile.deleteLines(tableFile, deletes, fileArr);
                        }
                    } else {
                        throw (SUCCESS);
                    }

                }
            }
            //改为先清缓存再抛成功
            await nodis.clearTableCache(['tableData', 'hash', 'bpTree'], tableName, database); //tableDefine可以不清除
            if ((ini.db.updateCache === true) && isTrans) {
                await this.refreshCache(['tableData', 'hash', 'bpTree'], database, tableName, tableFile, hashFile, bptFile);
            }
            if (!isTrans) {
                await Promise.all([
                    this.createBpTree(bptFile, tableFile, tableName, database),
                    this.createHash(tableFile, hashFile, tableDefine, tableName, database)
                ])
            }

            throw (SUCCESS);
        } catch (error) {
            //最后不解锁会死锁
            toLog(error);
            isTrans || await lock.getLock(database, tableName, 1, sign, null, null);
            if (isTrans) {
                throw ({code: error, trans: transInfo});
            }
            throw (error);
        }
    }

    //更新表
    async update(sql, sign, flag, isTrans, transInfo) {
        toLog("数据库更新语句为 ", sql);
        let database = sql.db;
        let tableName = sql.table;
        let asText = sql.as;
        let set = await this.getSet();
        database || (database = set.defaultDb);

        let paths = this.getFileNames(database, tableName, true);
        let tableFile = paths.tableFile;
        let hashFile = paths.hashFile;
        let bptFile = paths.bptFile;
        let firstTrans = false;
        if (isTrans && (!transInfo[database] || !transInfo[database][tableName])) {
            toLog(`初次执行事务 ,db = ${database} table = ${tableName}`);
            transInfo[database] = {};
            transInfo[database][tableName] = {
                tableData: null,
                tableDefine: null,
                hashIndex: null,
                bptFile: null,
                tableDetail: null,
                fileName: {
                    hashFile,
                    bptFile,
                    tableFile,
                }
            }
            firstTrans = true;
        }
        (isTrans && !firstTrans) || await lock.getLock(database, tableName, 0, sign, null, null);

        try {
            let tableDefine = null;
            let tableDetail = null;

            if (isTrans) {
                tableDefine = transInfo[database][tableName].tableDefine || await this.getTableDefine(tableName, tableFile, database);
                tableDetail = transInfo[database][tableName].tableDetail || await this.getTableKeyAndNum(tableDefine, tableName, database);
                transInfo[database][tableName].tableDefine = tableDefine;
                transInfo[database][tableName].tableDetail = tableDetail;
            } else {
                tableDefine = await this.getTableDefine(tableName, tableFile, database);
                tableDetail = await this.getTableKeyAndNum(tableDefine, tableName, database);
            }

            let key = tableDetail.key;
            let num = tableDetail.num;
            let where = {};
            //有内容再解析
            if (sql.where) {
                where = await this.parseWhere({}, sql.where);
            }
            toLog("where = ", JSON.stringify(where));
            // toLog("sql.set = ", sql.set);
            let setColumn = this.parseUpdateSet(sql.set);
            if ((where['AND'] && where['AND'][key] && where['AND'][key].operator == '=') && !where['OR']) {
                toLog("更新语句走哈希索引");
                let hashIndex = null;
                if (isTrans) {
                    hashIndex = transInfo[database][tableName].hashIndex || await this.getHashIndex(tableName, hashFile, database);
                } else {
                    hashIndex = await this.getHashIndex(tableName, hashFile, database);
                }
                let getIndex = where['AND'][key].value;
                toLog("getIndex = ", getIndex);
                let index = hashIndex[key][getIndex];
                if (!index) {
                    toLog("哈希索引未命中");
                    throw (SUCCESS);
                }
                //如果更新涉及到主键  则需要判断更新后是否冲突 反之则不可判断 必然会“冲突”
                if (hashIndex[key] && hashIndex[key]?.[setColumn[key]?.value]) {
                    throw ('KEY_EXIST');
                }
                delete hashIndex[key][getIndex];            //把老的索引删除
                let wholeFileArr = null;
                if (isTrans) {
                    wholeFileArr = transInfo[database][tableName].tableData || await this.getFileArrCache(tableName, tableFile, database);
                } else {
                    wholeFileArr = await this.getFileArrCache(tableName, tableFile, database);
                }
                toLog("wholeFileArr  = ", wholeFileArr);
                toLog("index = ", index);
                let lineText = wholeFileArr[index];
                toLog("lineText = ", lineText);
                let hit = await this.checkColumnAndData(lineText, where, tableDefine, num, true);
                toLog("hit = ", hit);
                if (!hit) {
                    throw (SUCCESS);
                }
                //到这里则说明行需要被修改
                let changed = await this.createUpdateLine(tableDefine, hit.parsedColumn, setColumn);
                let writeData = changed.dataStr;
                let checkData = changed.data;

                //是否命中主键
                if (setColumn[key]) {

                    hashIndex[key][checkData[key].value] = index;
                    wholeFileArr[index] = writeData;
                    if (isTrans) {
                        transInfo[database][tableName].tableData = wholeFileArr;
                        transInfo[database][tableName].hashIndex = hashIndex;
                    } else {
                        nthfile.writeFile(tableFile, wholeFileArr);
                        fs.writeFileSync(hashFile, JSON.stringify(hashIndex));
                    }


                } else {
                    wholeFileArr[index] = writeData;
                    if (isTrans) {
                        transInfo[database][tableName].tableData = wholeFileArr;
                    } else {

                        nthfile.writeFile(tableFile, wholeFileArr);
                    }

                }
            } else {
                toLog("全表搜索");
                let wholeFileArr = null;
                if (isTrans) {
                    wholeFileArr = transInfo[database][tableName].tableData || await this.getFileArrCache(tableName, tableFile, database);
                } else {
                    wholeFileArr = await this.getFileArrCache(tableName, tableFile, database);
                }

                let hitFlag = false;
                //20210205 放弃在缓存掐头去尾了 问题太多
                for (let i = 1; i < wholeFileArr.length - 1; i++) {
                    let hit = await this.checkColumnAndData(wholeFileArr[i], where, tableDefine, num, true);
                    if (hit) {
                        let temp = await this.createUpdateLine(tableDefine, hit.parsedColumn, setColumn);
                        wholeFileArr[i] = temp.dataStr;
                        hitFlag = true;
                    } else {
                        continue;
                    }
                }

                if (hitFlag) {
                    if (isTrans) {
                        transInfo[database][tableName].tableData = wholeFileArr;
                        transInfo[database][tableName].hashIndex = await this.createHash(tableFile, hashFile, tableDefine, tableName, database, true);
                        // transInfo[database][tableName].bptFile = await this.createBpTree(bptFile, tableFile, tableName, database,true);
                    } else {
                        nthfile.writeFile(tableFile, wholeFileArr);
                        await this.createHash(tableFile, hashFile, tableDefine, tableName, database);
                        // await this.createBpTree(bptFile, tableFile, tableName, database);
                    }
                    throw (SUCCESS);
                }
            }
            //结束
            await nodis.clearTableCache(['tableData', 'hash', 'bpTree'], tableName, database); //tableDefine可以不清除

            //如果配置了缓存策略则立即刷新缓存
            if (ini.db.updateCache === true) {
                toLog("立即刷新缓存");
                await this.refreshCache(['tableData', 'hash', 'bpTree'], database, tableName, tableFile, hashFile, bptFile);
            }

            if (isTrans) {
                transInfo[database][tableName].bptFile = await this.createBpTree(bptFile, tableFile, tableName, database, true);
            } else {
                await this.createBpTree(bptFile, tableFile, tableName, database);
            }

            // await nodis.show();
            throw (SUCCESS);
        } catch (error) {
            toLog(error);
            isTrans || await lock.getLock(database, tableName, 1, sign, null, null);
            if (isTrans) {
                throw ({code: error, trans: transInfo});
            }
            throw (error);
        }
    }

    //解析update语句中的set语句
    parseUpdateSet(set) {
        let res = {};
        for (let i = 0; i < set.length; i++) {
            //20210311 为了列长度和类型的改造 将这里的值转为数字或字符串
            if (set[i].value.type === 'string') {
                res[set[i].column] = set[i].value.value;
            } else {
                res[set[i].column] = parseFloat(set[i].value.value);
            }
        }
        return res;
    }


    //数据库查询逻辑实现
    async select(sql, sign, auth, flag, isTrans, transInfo) {
        toLog("传进来的sql 为 ", sql);
        //规避目前还不支持的SQL
        if (sql.groupby != null) {
            throw ('NO_GROUP_DIS');
        }
        //20210216 如果不是DISTINCT 暂时还不知道会有什么sql，先规避问题
        if (sql.distinct && sql.distinct !== 'DISTINCT') {
            throw ('SQL_NOT_SUPPORT');
        }

        let orderby = sql.orderby;
        let limit = sql.limit;
        let limitResult = -1;
        let distinct = sql.distinct;
        if (limit && limit.length) {
            for (let i = 0; i < limit.length; i++) {
                if (limit[i].type != 'number') {
                    throw ('SQL_NOT_SUPPORT');
                } else {
                    limitResult = limitResult > limit[i].value ? limitResult : limit[i].value;
                }
            }
        }
        limit = undefined;          //直接gc
        toLog("limitResult = ", limitResult);

        let orderByResult = [];     //要排序的列
        let orderRule = [];         //升序降序
        if (orderby && orderby.length) {
            for (let i = 0; i < orderby.length; i++) {
                //是为了杜绝and语句
                if (orderby[i].expr.type !== 'column_ref') {
                    throw ('SQL_NOT_SUPPORT');
                }
                orderByResult.push(orderby[i].expr.column);
                if (orderby[i].type === 'ASC' || orderby[i].type === 'asc') {
                    orderRule.push('asc');
                } else if (orderby[i].type === 'DESC' || orderby[i].type === 'desc') {
                    orderRule.push('desc');
                } else {
                    throw ('SQL_NOT_SUPPORT');
                }
            }
        }
        let set = await this.getSet();

        let database = sql.from[0].db || set.defaultDb;
        toLog("sign = ", sign);
        //false为数据库内部的查询 不进行权限校验
        if (auth !== false) {
            this.checkAuth(database, sign);
        }
        let tableName = sql.from[0].table;
        let paths = this.getFileNames(database, tableName, true);
        let tableFile = paths.tableFile;
        let hashFile = paths.hashFile;
        let bptFile = paths.bptFile;
        let firstTrans = false;
        if (isTrans && (!transInfo[database] || !transInfo[database][tableName])) {
            toLog(`初次执行事务 ,db = ${database} table = ${tableName}`);
            transInfo[database] = {};
            transInfo[database][tableName] = {
                tableData: null,
                tableDefine: null,
                hashIndex: null,
                bptFile: null,
                tableDetail: null,
                fileName: {
                    hashFile,
                    bptFile,
                    tableFile,
                }
            }
            firstTrans = true;
        }
        (isTrans && !firstTrans) || await lock.getLock(database, tableName, 0, sign, null, null);

        try {
            let tableDefine = null;
            let tableDetail = null;

            if (isTrans) {
                tableDefine = transInfo[database][tableName].tableDefine || await this.getTableDefine(tableName, tableFile, database);
                tableDetail = transInfo[database][tableName].tableDetail || await this.getTableKeyAndNum(tableDefine, tableName, database);
                transInfo[database][tableName].tableDefine = tableDefine;
                transInfo[database][tableName].tableDetail = tableDetail;
            } else {
                tableDefine = await this.getTableDefine(tableName, tableFile, database);
                tableDetail = await this.getTableKeyAndNum(tableDefine, tableName, database);
            }

            toLog("tableDefine = ", tableDefine);
            let key = tableDetail.key;
            let num = tableDetail.num;

            let selectColumns = sql.columns;        //是一个数组
            toLog("selectColumns = ", selectColumns);
            //遇到通配符就转成数组
            if (selectColumns === '*') {
                selectColumns = [];
                for (let key in tableDefine.table) {
                    let selectTemp = {};
                    selectTemp.as = null;       //as必然是null
                    selectTemp.expr = {
                        type: 'column_ref',
                        table: '',
                        column: key
                    }
                    selectColumns.push(selectTemp);
                }
            }

            let where = {};
            //有内容再解析
            let res = [];
            if (sql.where) {
                where = await this.parseWhere({}, sql.where);
            }
            toLog("where = ", JSON.stringify(where));
            if ((where['AND'] && where['AND'][key] && where['AND'][key].operator === '=') && !where['OR']) {
                toLog("select操作命中主键");

                let hashIndex = null;
                if (isTrans) {
                    hashIndex = transInfo[database][tableName].hashIndex || await this.getHashIndex(tableName, hashFile, database);
                    transInfo[database][tableName].hashIndex = hashIndex;
                } else {
                    hashIndex = await this.getHashIndex(tableName, hashFile, database);
                }

                let index = (where['AND'] && where['AND'][key]) ? hashIndex[key][where['AND'][key].value] : hashIndex[key][where['OR'][key].value];
                toLog("要查询的记录在 ", index);
                //主键不存在直接代表数据不存在
                if (!index && index !== 0) {
                    throw ({code: SUCCESS, data: res});
                }
                let lineText = await readIndex(index, tableFile);
                toLog("lineText = ", lineText);
                //这里肯定不能掐头去尾 只有全表搜索才需要啊
                res = await this.combineSelectedData(selectColumns, [lineText], where, tableDefine, num, undefined, false);
            } else if ((where['AND'] && where['AND'][key] && where['AND'][key].operator !== '=' && where['AND'][key].operator !== 'LIKE') && !where['OR'] && ifBpt) {
                //尝试用上B+树 20210208 like语句不能使用索引
                toLog("使用B+树索引");
                let btree = null;
                if (isTrans) {
                    //以下代码实现将B+树从文件中还原 并重构节点
                    btree = transInfo[database][tableName].bptFile || await this.restoreBpTree(bptFile, tableName, database, tableDetail);
                    transInfo[database][tableName].bptFile = btree;
                } else {
                    btree = await this.restoreBpTree(bptFile, tableName, database, tableDetail);
                }
                let low = {}, high = {};
                low[key] = null;
                high[key] = "max";      //默认赋值max 不然啥都查不到
                let flag = where['AND'] ? 'AND' : 'OR';
                if (where[flag][key].operator === '>') {
                    low[key] = (parseFloat(where[flag][key].value) - 1).toString();
                }
                if (where[flag][key].operator === '>=') {
                    low[key] = where[flag][key].value;
                }
                if (where[flag][key].operator === '<') {
                    high[key] = where[flag][key].value;
                }
                if (where[flag][key].operator === '<=') {
                    high[key] = (parseFloat(where[flag][key].value) + 1).toString();
                }
                toLog(`B+树搜索 low = ${JSON.stringify(low)},high = ${JSON.stringify(high)}`);
                let selectBpt = btree.getRange(low, high);
                toLog("selectBpt = ", JSON.stringify(selectBpt));
                let cleaned = await this.convertBpTreeResult(selectBpt);
                res = await this.combineSelectedData(selectColumns, cleaned, where, tableDefine, num, false, false);
            } else {
                toLog("全表搜索");
                let arr = null;
                if (isTrans) {
                    arr = transInfo[database][tableName].tableData || await this.getFileArrCache(tableName, tableFile, database);
                    transInfo[database][tableName].tableData = arr;

                } else {
                    arr = await this.getFileArrCache(tableName, tableFile, database);
                }
                toLog("arr = ", arr);
                // arr = this.breakBothEnd(arr);

                res = await this.combineSelectedData(selectColumns, arr, where, tableDefine, num, undefined, true);
            }

            //     //暂时只支持count(*)吧
            if (orderByResult.length || orderRule.length) {
                res = _.sortBy(res, orderByResult, orderRule);
            }
            if (limitResult != -1) {
                res.length = res.length < limitResult ? res.length : limitResult;       //limit的长度两者取最小
            }
            //20210217 加入distinct处理
            if (sql.distinct) {
                res = await this.doDistinct(res, selectColumns, tableDetail);
            }
            throw ({code: SUCCESS, data: res});
        } catch (error) {
            toLog(error);
            isTrans || await lock.getLock(database, tableName, 1, sign, null, null);
            if (isTrans && (error === SUCCESS || error.code === SUCCESS)) {
                error.trans = transInfo;
                throw (error);
            }
            throw (error);
        }
    }

    //删除B+树查询结果里的多余数据 转换成想要的结果
    //例子 [{"a":"14348"},{"a":"14348","b":"77598","c":"30911","d":"2723","e":"72410"}]
    //保留[{"a":"14348","b":"77598","c":"30911","d":"2723","e":"72410"}]
    async convertBpTreeResult(arr) {
        let res = [];
        for (let i = 0; i < arr.length; i++) {
            if (arr[i][1]) {
                let temp = {};
                for (let key in arr[i][1]) {
                    temp[key] = {
                        value: arr[i][1][key],
                    };
                }
                res.push(temp);
            }
        }
        return res;
    }

    //组装查询到的数据  lineArr是数组
    async combineSelectedData(selectColumns, lineArr, where, tableDefine, num, needParse, ifBreak) {
        let res = [];
        let count = 0;
        let ifSubtraction = false;      //默认不需要减法
        let begin = ifBreak ? 1 : 0;
        let end = ifBreak ? lineArr.length - 1 : lineArr.length;
        for (let k = begin; k < end; k++) {
            ifSubtraction = false;          //每一轮循环都初始化一下
            if (ifObjectIsEmpty(lineArr[k])) {
                continue;
            }
            let result = await this.checkColumnAndData(lineArr[k], where, tableDefine, num, true, needParse);
            if (!result) {
                continue;
            }
            //20210129 count语法支持count某些字段,count(a,b,c)则需要每个字段的值都不为null才行

            count++;
            let parsedColumn = result.parsedColumn;
            let temp = {};      //入数组的元素 组成 { key : value , key1:value} 的形式
            for (let i = 0; i < selectColumns.length; i++) {
                let left = selectColumns[i].as || selectColumns[i].expr.column || selectColumns[i].expr.name;     //有as就取as，否则取column原生字段
                //先判断有没有COUNT！
                if (selectColumns[i].expr.type === 'aggr_func' && selectColumns[i].expr.name === 'COUNT' || selectColumns[i].expr.name === 'count') {
                    res = [];
                    if (selectColumns[i].expr.args.type === 'expr_list') {
                        let values = selectColumns[i].expr.args.value;
                        for (let k = 0; k < values.length; k++) {
                            if (values[k].type === 'column_ref') {
                                //先判断列是否存在，再判断是否为空
                                if (values[k].table) {
                                    throw ("COUNT_NO_TABLE");
                                }

                                if (!parsedColumn[values[k].column]) {
                                    toLog("!parsedColumn[values[k].column");
                                    throw ("COLUMN_NOT_FOUND");
                                }

                                if ((parsedColumn[values[k].column].value === null) && !ifSubtraction) {
                                    ifSubtraction = true;
                                }
                            }
                            //剩下的就是number类型
                        }
                    } else if (selectColumns[i].expr.args.expr.type === 'column_ref') {
                        if (selectColumns[i].expr.args.expr.table) {
                            throw ("COUNT_NO_TABLE");
                        }
                        let countColumn = selectColumns[i].expr.args.expr.column;
                        if (!parsedColumn[countColumn]) {
                            toLog("!parsedColumn[countColumn]");
                            throw ("COLUMN_NOT_FOUND");
                        }

                        if (parsedColumn[countColumn].value === null) {
                            ifSubtraction = true;
                        }
                    }

                    !ifSubtraction || count--;
                    temp[left] = count;
                    break;
                }
                // '{"expr":{"type":"function","name":"count","args":{"type":"expr_list","value":[{"type":"column_ref","table":"","column":"a"},{"type":"column_ref","table":"","column":"g"}]}},"as":null}'
                //先处理查询的列重复的问题
                if (temp[selectColumns[i].expr.column]) {
                    throw ("COLUMN_REPEAT");
                }
                //再处理列不在表中的问题
                if (!parsedColumn[selectColumns[i].expr.column]) {
                    toLog("!parsedColumn[selectColumns[i].expr.column]");
                    throw ("COLUMN_NOT_FOUND");
                }
                temp[left] = parsedColumn[selectColumns[i].expr.column].value;
            }
            res.push(temp);
        }
        return res;
    }

    //根据先前写入的数据和表结构 对该行数据进行解析
    columnParse(tableDefine, column, columnNum) {
        let columns = tableDefine.table;
        toLog("column = ", column);
        let arr = column.split(WHITE_SPACE);
        let ifNull = libcu.tools.padZero(parseInt(arr[0], 16), columnNum);
        let i = 0;    //解析用

        //然后按照行定义将数据解析
        for (let key in columns) {
            let locate = columns[key].locate;
            //当这个下标有数据的时候 下标往前移动一次 注意需要跳过第一个压缩字节
            if (ifNull[locate] === 1) {
                columns[key].value = arr[i + 1];
                i++;
            } else {
                columns[key].value = null;
            }
        }
        return columns;
    }

    //根据先前写入的数据和表结构 对该行数据进行解析 然后只返回{key:value}
    columnParseSimple(tableDefine, column, columnNum) {
        let columns = tableDefine.table;
        column = column.replace(EOL, "");
        let arr = column.split(WHITE_SPACE);
        let ifNull = libcu.tools.padZero(parseInt(arr[0], 16), columnNum);
        let i = 0;    //解析用
        let temp = {};
        //然后按照行定义将数据解析
        for (let key in columns) {
            let locate = columns[key].locate;
            //当这个下标有数据的时候 下标往前移动一次 注意需要跳过第一个压缩字节
            if (ifNull[locate] === 1) {
                temp[key] = arr[i + 1];
                i++;
            } else {
                temp[key] = null;
            }
        }
        return temp;
    }


    //根据表的数据重构哈希索引 结构 {key : {value : index }}
    async createHash(tableFile, hashFile, tableDefine, tableName, dbName, dontWrite) {
        let tableDetail = await this.getTableKeyAndNum(tableDefine, tableName, dbName);
        let key = tableDetail.key;
        let num = tableDetail.num;
        let hash = {};
        hash[key] = {};     //先初始化哈希索引
        let arr = await this.getFileArrCache(tableName, tableFile, dbName);
        //20201227 getFileArrCache 已经将数据掐头去尾了，所以这里必须要从0开始 Holy crap
        //20210206 重新分析一下
        for (let i = 0; i < arr.length; i++) {
            toLog(" 创建哈希拿到的数据为 ", arr[i]);
            if (!arr[i]) {
                break;
            }
            let parseResult = this.columnParse(tableDefine, arr[i], num);

            hash[key][parseResult[key].value] = i;
        }
        if (dontWrite) {
            return hash;
        } else {
            fs.writeFileSync(hashFile, JSON.stringify(hash));
            trace.setLog("已重建表哈希索引");
        }
    }


    //本函数实现生成默认的B+树结构 并从文件内还原B+树
    async restoreBpTree(bptFile, tableName, database, tableDetail) {
        //以下代码实现将B+树从文件中还原 并添加新的节点
        let btree = this.initBpTree(tableDetail.key);
        let btreeTemp = await this.getBPTreeFromCache(bptFile, tableName, database);
        btree._root.keys = btreeTemp ? btreeTemp._root.keys : btree._root.keys;
        btree._root.values = btreeTemp ? btreeTemp._root.values : btree._root.values;
        btree._size = btreeTemp ? btreeTemp._size : btree._size;

        return btree;
    }


    //传原生的行数据和sql里面的列数据，返回这个行数据是否被命中 return true/false
    async checkColumnAndData(rawColumn, sqlColumn, tableDefine, columnNum, ifObject, dontParse) {
        //20201223 增加参数 判断需不需要解析源数据
        let parsedColumn = null;
        if (dontParse === true || dontParse === undefined) {
            parsedColumn = this.columnParse(tableDefine, rawColumn, columnNum);
        } else {
            parsedColumn = rawColumn;
        }

        //判断sql里面的列在不在解析的行数据里面 注意不要搞反了
        let AND = sqlColumn.AND;
        let OR = sqlColumn.OR;
        //OR和AND是反逻辑 一旦命中就直接return
        //先判断OR 有命中直接return 会增加效率
        if (OR) {
            for (let key in OR) {
                let operator = OR[key].operator;
                let value = parseFloat(OR[key].value);
                let tableValue = parseFloat(parsedColumn[key].value);

                if (!value || !tableValue) {
                    toLog("!value || !tableValue");
                    return false;
                }
                if (operator === '=') {
                    //因为isNaN不会判断空字符串和null
                    if (tableValue === value) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else if (operator === '>') {
                    if (tableValue > value || isNaN(tableValue) || isNaN(value)) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else if (operator === '<') {
                    if (tableValue < value || isNaN(tableValue) || isNaN(value)) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                }
                // 20201223 增加对 >= <= <> !=的支持
                else if (operator === '<=') {
                    if (tableValue <= value || isNaN(tableValue) || isNaN(value)) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else if (operator === '>=') {
                    if (tableValue >= value || isNaN(tableValue) || isNaN(value)) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else if (operator === '<>') {
                    if (tableValue !== value || isNaN(tableValue) || isNaN(value)) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else if (operator === '!=') {
                    if (tableValue !== value || isNaN(tableValue) || isNaN(value)) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                }

                //20210203 增加对IN OR操作符的支持
                else if (operator === 'IN') {
                    //需要把零碎的数据去掉
                    if (libcu.tools.contains(value, tableValue, "value")) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else if (operator === 'LIKE') {
                    if (tableValue.toString().indexOf(value) !== -1) {
                        if (ifObject) {
                            return {
                                result: true,
                                parsedColumn,
                            }
                        } else {
                            return true;
                        }
                    }
                } else {
                    toLog('checkColumnAndData 进入了未知的case ,tableValue = ', tableValue, " value = ", value, ' operator = ', operator);
                    trace.setLog(`检测到暂不支持的SQL语法 operator:${operator} `);
                    throw ("SQL_NOT_SUPPORT");
                }
            }
        }


        if (AND) {
            for (let key in AND) {

                if (!parsedColumn[key]) {
                    toLog("checkColumnAndData !parsedColumn[key]")
                    throw ('COLUMN_NOT_FOUND');
                }
                let operator = AND[key].operator;
                let value = parseFloat(AND[key].value);
                let tableValue = parseFloat(parsedColumn[key].value);
                //20210319 一定要判断下是否为0啊我的天
                if ((!value || !tableValue) && (value !== 0 && tableValue !== 0)) {
                    toLog("!value || !tableValue");
                    return false;
                }
                if (operator === '=') {
                    //因为isNaN不会判断空字符串和null
                    if (tableValue !== value) {
                        toLog("tableValue != value");
                        return false;
                    }
                } else if (operator === '>') {
                    if (tableValue <= value || isNaN(tableValue) || isNaN(value)) {
                        toLog("tableValue <= value || isNaN(tableValue) || isNaN(value)")
                        return false;
                    }
                } else if (operator === '<') {
                    if (tableValue >= value || isNaN(tableValue) || isNaN(value)) {
                        toLog("tableValue >= value || isNaN(tableValue) || isNaN(value)")
                        return false;
                    }
                }
                // 20201223 增加对 >= <= <> !=的支持
                else if (operator === '<=') {
                    if (tableValue > value || isNaN(tableValue) || isNaN(value)) {
                        toLog("tableValue > value || isNaN(tableValue) || isNaN(value)")
                        return false;
                    }
                } else if (operator === '>=') {
                    if (tableValue < value || isNaN(tableValue) || isNaN(value)) {
                        toLog("tableValue < value || isNaN(tableValue) || isNaN(value)")
                        return false;
                    }
                } else if (operator === '<>') {
                    if (tableValue === value || isNaN(tableValue) || isNaN(value)) {
                        toLog("tableValue == value || isNaN(tableValue) || isNaN(value)")
                        return false;
                    }
                } else if (operator === '!=') {
                    if (tableValue === value || isNaN(tableValue) || isNaN(value)) {
                        toLog("tableValue == value || isNaN(tableValue) || isNaN(value)")
                        return false;
                    }
                }
                //20210203 增加对IN OR操作符的支持
                else if (operator === 'IN') {
                    //需要把零碎的数据去掉
                    if (!libcu.tools.contains(value, tableValue, "value")) {
                        return false;
                    }
                } else if (operator === 'LIKE') {
                    //模糊查询
                    if (tableValue.toString().indexOf(value) === -1) {
                        return false;
                    }
                } else {
                    toLog('checkColumnAndData 进入了未知的case ,tableValue = ', tableValue, " value = ", value, ' operator = ', operator);
                    throw ("SQL_NOT_SUPPORT");
                }
            }
        }
        //如果OR没命中 还不存在AND 直接返回false
        if (OR && !AND) {
            toLog("OR && !AND")
            return false;
        }

        //校验全通过了就返回true
        if (ifObject) {
            return {
                result: true,
                parsedColumn,
            }
        } else {
            return true;
        }
    }


    //用于update语句写入行 parsedColumn 是已被解析的原数据,sqlColumn是sql语句要修改的列

    // 预定义的入库数据为  {
    //     a: { value: '93812' },
    //     b: { value: '32312' },
    //     c: { value: '01927' },
    //     d: { value: '93795' },
    //     e: { value: '41139' }
    //   }
    async createUpdateLine(tableDefine, parsedColumn, sqlColumn) {
        toLog("createUpdateLine sqlColumn = ", sqlColumn);
        for (let key in sqlColumn) {
            if (!parsedColumn[key]) {
                toLog("createUpdateLine !parsedColumn[key]");
                throw ('COLUMN_NOT_FOUND');
            }
            //把要修改的值赋上
            //20210311 增加类型判断
            parsedColumn[key].value = parseFloat(sqlColumn[key]);
        }
        for (let key in parsedColumn) {
            if (parsedColumn[key].type === 'number') {
                parsedColumn[key].value = parseFloat(parsedColumn[key].value);
            }
        }
        return await this.createLine(tableDefine, parsedColumn, true);
    }

    async createLine(tableDefine, data, noEOL) {
        let tablecolumn = tableDefine.table;
        let ifNull = '';        //数据行第一行 表示是否有空元素
        let dataStr = '';       //数据行压缩元素
        toLog("tableDefine = ", JSON.stringify(tableDefine));
        for (let key in data) {

            //列非空检查失败
            if (!data[key].value && (tablecolumn[key].not === 'null')) {
                throw ('COLUMN_NOT_NULL');
            }
            //列not check失败
            if ((data[key].value === tablecolumn[key].not) && tablecolumn[key].not !== null) {
                throw ('COLUMN_NOT_CHECK');
            }

            //判断数据是否超过长度
            this.checkLenAndType(tableDefine, key, data[key].value, data[key].type);
            //压缩元素规则，不为空就是1，空则为0
            if (data[key].value != null) {
                ifNull += '1';
                dataStr += WHITE_SPACE + data[key].value;
            } else {
                ifNull += '0';
            }
        }

        //压缩成16进制数
        ifNull = parseInt(ifNull).toString(16);

        //有些情况要换行符 有些不要
        if (noEOL) {
            dataStr = ifNull + dataStr
        } else {
            dataStr = ifNull + dataStr + EOL;
        }
        return {
            dataStr,
            data
        };
    }

    //检查数据列是否类型不匹配 或者超过长度 成功就return 不成功throw相应的错误
    checkLenAndType(tableDefine, key, value) {
        if (value === null) {
            return;
        }
        let type = tableDefine.table[key].type;
        let len = tableDefine.table[key].len;
        let isNumber = dataTypes[type].isNumber;
        toLog("isNumber = ", isNumber);
        len = (len !== '') ? len : dataTypes[type].len;
        toLog("len = ", len);
        //先判断数字和字符串类型错误问题
        //是数字类型但是传进来的值不是数字
        if (isNumber && typeof value !== 'number') {
            throw ("COLUMN_TYPE_ERROR");
        }
        if (!isNumber && typeof value !== 'string') {
            throw ("COLUMN_TYPE_ERROR");
        }
        if (isNumber) {
            toLog("value.toString().length = ", value.toString().length);
            if (value.toString().length > len) {
                throw ('COLUMN_OUT_OF_LENGTH');
            }
        } else {
            toLog("value.length = ", value.length);
            if (value.length > len) {
                throw ('COLUMN_OUT_OF_LENGTH');
            }
        }
    }

    //递归解析多个where 和 and时的数据 20210203 增加操作符
    async parseWhere(result, where, lastOperator) {
        if (!lastOperator) {
            //规避条件里面没有and和or 只有一个where的情况
            lastOperator = "AND";
        }
        //用递归
        if (where && (where.operator === 'AND' || where.operator === 'and' || where.operator === 'OR' || where.operator === 'or')) {
            await this.parseWhere(result, where.left, where.operator);
            await this.parseWhere(result, where.right, where.operator);
            return result;
        } else {
            //20210203 将操作符归类 OR AND 分组
            if (!result[lastOperator]) {
                result[lastOperator] = {};
            }
            //20210204 这个不加还是得出问题 加吧 不然就被覆盖了
            if (result[lastOperator][where.left.column]) {
                throw ('COLUMN_REPEAT');
            }
            result[lastOperator][where.left.column] = {
                operator: where.operator,
                value: where.right.value,
            }
            return result;
        }
    }

    //根据传入的数组 刷新缓存
    async refreshCache(arr, dbName, tableName, tableFile, hashFile, bptFile) {
        toLog("开始刷新缓存 处理的对象为 ", arr);
        for (let i = 0; i < arr.length; i++) {
            //感觉后期可以用promiseAll优化
            if (arr[i] === 'tableDetail') {
                await this.getTableKeyAndNum(tableName, tableFile, dbName);
            } else if (arr[i] === 'hash') {
                await this.getHashIndex(tableName, hashFile, dbName);
            } else if (arr[i] === 'tableData') {
                await this.getFileArrCache(tableName, tableFile, dbName);
            } else if (arr[i] === 'tableDefine') {
                await this.getTableDefine(tableName, tableFile, dbName);
            } else if (arr[i] === 'bpTree') {
                await this.getBPTreeFromCache(bptFile, tableName, dbName);
            } else {
                throw ("UNKNOWN_ERROR");
            }
        }
    }

    //展示目前所有的数据库名
    async showDatabase() {
        let dbPath = ini.main.path;
        let files = await libcu.cf.walkFolder(dbPath);
        let dirs = files.dirList;
        for (let i = 0; i < dirs.length; i++) {
            dirs[i] = path.basename(dirs[i]);
        }
        throw ({code: SUCCESS, data: dirs});
    }

    //展示目前所有的表名
    async showTable(database) {
        let dbSet = await this.getSet();
        database = database || dbSet.defaultDb;
        let dbPath = path.join(ini.main.path, database);
        if (!fs.existsSync(dbPath)) {
            throw ("DATABASE_NOT_FOUND");
        }
        let all = await libcu.cf.walkFolder(dbPath);
        let files = all.fileList;
        let res = [];
        let tableName = ini.name.table;
        tableName = tableName.replace('.', "");
        for (let i = 0; i < files.length; i++) {
            let fileRawName = path.basename(files[i]);
            let arr = fileRawName.split('.');
            if (arr[1] === tableName) {
                res.push(arr[0]);
            }
        }
        throw ({code: SUCCESS, data: res});
    }

    //掐头去尾
    breakBothEnd(arr) {
        if (!arr || !arr.length) {
            return arr;
        }
        arr.splice(0, 1);
        arr.splice(arr.length - 1, 1);
        return arr;
    }

    //处理distinct的函数 20210222 考虑用多叉树来处理
    async doDistinct(data, columns, tableDetail) {
        let mulTree = new MultipleTree();
        if (columns.length === 1 && columns[0].expr.column === tableDetail.key) {
            //如果是主键的话本身就是distinct了
            toLog("优化器跳过distinct");
            return data;
        }

        //遍历结果集 剔除重复的列
        for (let i = 0; i < data.length; i++) {
            let line = data[i];
            if (mulTree.insert(line) === false) {
                data.splice(i, 1);
                i--;
            }
        }
        return data;
    }


}

module.exports = Core;
