const Btree = require('sorted-btree').default;

//初始化B+树 调用其构造函数 一个示例函数
exports.initBtree = () => {
    let btree = new Btree(undefined, (a, b) => {
        if (a.a > b.a) {
            return 1;
        } else if (a.a < b.a) {
            return -1;
        } else {
            return a.a - b.a
        }
    });
    return btree;
}

