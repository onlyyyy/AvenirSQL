const { SUCCESS, WHITE_SPACE } = require('./common/constants');
const { toLog } = require('./common/common');

//AvenirSQL的SQL解析模块
const parse = require('node-sqlparser').parse;
class SQL {
    constructor() {

    }

    parseSql(sql) {
        trace.setLog(`接收到的sql为[${sql}]`);
        //20210218 SQL中规避分隔符问题 就是SQL不允许带分隔符
        if (sql.indexOf(WHITE_SPACE) != -1) {
            throw ("WHITE_SPACE_ERROR");
        }
        try {
            const res = parse(sql);
            toLog("sql parse -> ", res);
            return res;
        } catch (e) {
            toLog("sql parse error - >", e);
            log.error('error sql :', sql);
            throw ('SQL_PARSE_ERROR');
        }
    }

    //解析AvenirSQL
    async parseAvenirSql(array, rawSql, sign) {
        const jump = () => {
            throw ('error');        //这个函数不是真正的error，只是要告诉外层函数去转交给sqlParser处理
        }
        const fail = () => {
            log.error("error sql :", rawSql);
            throw ('SQL_PARSE_ERROR');
        }
        if (array.length > ini.db.maxSqlLength) {
            throw ('SQL_TOO_LONG');
        }
        switch (array[0]) {
            case 'use':
                //解析use xxx命令
                if (array.length !== 2) {
                    jump();
                } else {
                    await db.useDB(array[1]);
                }
                break;
            case 'create':
                //解析create database 命令
                if (array[1] === 'database') {
                    let res = await db.createDb(array[2]);
                    return res;
                } else if (array[1] === 'user') {
                    //解析create user 命令 create user name password auth  用户名 密码
                    if (array.length != 4 && array.length != 5) {
                        fail();
                    } else {
                        let name = array[2];
                        let password = array[3];
                        let auth = array[4];
                        //在这里先判断用户名和密码是否正确？
                        if (conn.isRoot(sign)) {
                            await user.createUser(name, password, auth, sign);
                        } else {
                            throw ('PERMISSION_DENIED');
                        }


                    }
                } else {
                    jump();
                }

                break;
            case 'drop':
                //drop database
                if (array.length !== 3) {
                    toLog("array.length = ", array.length);
                    throw ('SQL_PARSE_ERROR');
                }
                if (array[1] === 'database') {
                    await db.dropDatabase(array[2]);
                } else if (array[1] === 'table') {
                    await db.dropTable(array[2], sign);
                } else {
                    fail();
                }
                break;
            case 'show':
                //20201229 show database,show table
                if (array.length !== 2 && array.length !==3) {
                    throw ('SQL_PARSE_ERROR');
                }
                if (array[1] === 'database') {
                    await db.showDatabase();
                } else if (array[1] === 'table') {
                    await db.showTable(array[2]);
                } else if (array[1] === 'dump') {
                    await dump.showDumpList();
                } else {
                    throw ('SQL_PARSE_ERROR');
                }
                break;
            case 'dump':
                if (array[1] === 'database') {
                    await dump.dumpDatabase();
                } else if (array[1] === 'table') {
                    //导出表
                    let DBTable = array[2] || fail();
                    DBTable = DBTable.split('.');
                    let database = '';
                    let tableName = '';
                    if (DBTable.length > 2 || DBTable.length < 1) {
                        throw ('DUMP_SQL_ERROR');
                    } else {
                        if (DBTable.length === 2) {
                            database = DBTable[0];
                            tableName = DBTable[1];
                        } else {
                            tableName = DBTable[0];
                            database = await db.getSet();
                            database = database.defaultDb;
                        }
                    }
                    toLog("database = ", database);
                    let sql = '';
                    if (array.length > 3) {
                        //语法必须带上as 或者搜索某些表
                        (array[4] === 'select') || fail();
                        (array[3] === 'as') || fail();
                        for (let i = 4; i < array.length; i++) {
                            sql += array[i];
                            sql += ' ';
                        }

                    } else {
                        sql = `select * from ${tableName}`;
                    }
                    let parsed = this.parseSql(sql);
                    if (parsed.from[0].table !== tableName) {
                        throw ('DUMP_TABLENAME_ERROR');
                    }
                    if (parsed.type !== 'select') {
                        throw ('DUMP_SQL_ERROR');
                    }
                    await dump.dumpTable(tableName, database, parsed, sign);
                } else {
                    fail();
                }
                break;
            //执行sql文件
            case 'exec':
                let fileName = array[1] || fail();
                await dump.exec(fileName,sign);
                break;
            case 'ping':
                //验证数据库程序是否存活
                throw({
                    code:SUCCESS,
                    data:'pong'
                })
            case 'set':
                // 获取设置或者改变数据库的设置
                console.log('array = ',array);
                if(array.length === 1) {
                    throw({
                        code:SUCCESS,
                        data:ini,
                    })
                } else if (array.length <= 4) {
                    await db.doSet(array[1],array[2],array[3]);
                } else {
                    throw('SET_SQL_ERROR');
                }
                break;
            default:
                //随便throw一个东西就行了
                jump();
        }
    }


    //包含原生SQL和能够被AvenirSQL识别的语句
    async parse(sql, sign) {
        //先解析AvenirSQL特有的语句 再解析原生SQL
        toLog("要解析的 sql为 ", sql);
        if (!sql) {
            throw ('SQL_PARSE_ERROR');
        }
        let raw = this.getArray(sql);
        if (raw.length === 0) {
            throw ('SQL_PARSE_ERROR');
        } else {
            //AvenirSQL解析出错不报错，转给解析器解析，解析器报错直接throw
            try {
                await this.parseAvenirSql(raw, sql, sign);
            } catch (error) {
                //不是内部定义的错误就代表程序处理出错了
                toLog('error = ', error);
                if (error === SUCCESS || error != 'error') {
                    throw (error);
                }
                //不需要try catch了，底层会抓住错误
                let par = this.parseSql(sql);
                await this.doSql(par, sign);
            }
        }
    }

    //sql转化为数组
    getArray(sql) {
        if (!sql) {
            throw ('INVALID_SQL_ERROR');
        }
        let res = sql.split(' ');
        //20210804 去掉纯粹的括号 这个没用
        for(let i=0;i<res.length;i++) {
            if(_.trim(res[i]) === '') {
                res.splice(i,1);
                i--;
            } else {}
            res[i] = _.trim(res[i]);
        }
        return res;
    }

    //根据解析出来的sql进行一系列操作
    async doSql(par, sign, auth) {
        if (par.type === 'create_table') {
            await db.createTable(par, sign, auth);
        } else if (par.type === 'insert') {
            await db.insert(par, sign, auth);
        } else if (par.type === 'delete') {
            await db.delete(par, sign, auth);
        } else if (par.type === 'select') {
            //试图开发join
            await db.select(par, sign, auth);
        } else if (par.type === 'update') {
            await db.update(par, sign, auth);
        }
    }

}
module.exports = SQL;
