//数据库日志模块

const { EOL } = require("./common/common");
const moment = require('moment');
class Trace {
    constructor() {
        this.loopTime = (ini.log.loopTime > 100 || ini.log.loopTime < 10) ? 10 : ini.log.loopTime;
        this.logText = [];              //要写入日志的内容     
        setInterval(async () => {
            this.writeLog();
        }, this.loopTime * 1000);
    }

    writeLog() {
        let text = '';
        for (let i = 0; i < this.logText.length; i++) {
            text += this.logText[i] + EOL;
        }
        if (!text || text * 1 === 0) {
            return;
        }
        log.log(text);
        this.logText = [];      //写入之后清理
    }

    setLog(text) {
        //精确到毫秒 为了避免高并发下日志时间格式不全的问题
        let time = moment().format("HH:mm:ss.SSS");
        text = time + ' ' + text;
        this.logText.push(text);
    }
}

module.exports = Trace;