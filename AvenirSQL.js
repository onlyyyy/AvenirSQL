// AvenirSQL
const Socket = require('./tcp/socket');
const Core = require('./database/Core');
const User = require('./database/User');
const os = require('os');
const fs = require('fs');
const INI = require('ini');
const path = require('path');
const Logger = require('avenir-log');
const constants = require('./database/common/constants');
const Conn = require('./database/Conn');
const Nodis = require('./database/Nodis');
const _ = require('lodash');
const nthFile = require('nth-file');
const Lock = require('./database/Lock');
const SQL = require('./database/Sql');
const Trans = require('./database/Trans');
const Trace = require('./database/Trace');
const Dump = require('./database/Dump');
const set = require('./database/common/set');
async function start() {
    Bless();
    let file = await giveFileName();
    //如果没有ini文件就自创一个
    if (!fs.existsSync(file)) {
        defaultIni();
    }
    let ini = await readIni(file);
    global.ini = ini;
    let socket = new Socket();
    let sql = new SQL();
    let core = new Core();
    let user = new User();
    let log = new Logger(ini.log.logName, ini.log.logPath);
    let conn = new Conn();
    //不希望没事就写日志 上生产了再说
    ini.db.debug || log.noDebug();
    global.log = log;
    global.socket = socket;
    //为了防止名称冲突
    global.define = {
        fileName: file,
    }
    global.db = core;       //core 名称冲突
    global.sql = sql;
    global.user = user;
    global.conn = conn;
    //nodis一定要创建在ini的后面 因为用到了ini的定义
    global.nodis = new Nodis();
    let lock = new Lock();
    global._ = _;
    global.lock = lock;
    global.trans = new Trans();
    global.trace = new Trace();
    global.nthFile = nthFile;
    global.dump = new Dump();
    await socket.init(ini.main.ip, ini.main.port);//启动sock

    dbInit();
}

async function main() {
    await start();

}

async function giveFileName() {
    let type = os.platform();
    let fileName;
    if (type === "darwin" && constants.ifSingle) {
        fileName = "/Users/hideyoshi/Desktop/codes/avenir-db/run.ini";

    }
    else if (type === "win32" && constants.ifSingle) {
        fileName = "F:/avenir-db/run.ini";

    }
    else {
        fileName = "./run.ini"

    }
    return fileName;
}


async function readIni(fileName) {
    let file = fs.readFileSync(fileName);
    return INI.parse(file.toString());
}


//初始化数据库配置信息
function dbInit() {
    let fileName = ini.name.rootSet;
    fileName = path.join(ini.main.path, fileName);
    if (!fs.existsSync(ini.main.path)) {
        fs.mkdirSync(ini.main.path, { recursive: true });
    }
    if (!fs.existsSync(fileName)) {
        fs.writeFileSync(fileName, '{}');        //默认写一个空的配置
    }
}

//默认的配置
function defaultIni() {
    fs.writeFileSync('./run.ini', INI.stringify(set.defaultSet));
}

function Bless() {
    console.log(`
       ┌─┐       ┌─┐
    ┌──┘ ┴───────┘ ┴──┐
    │                 │
    │       ───       │
    │  ─┬┘       └┬─  │
    │                 │
    │       ─┴─       │
    │                 │
    └───┐         ┌───┘
        │         │
        │         │
        │         │
        │         └──────────────┐
        │                        │
        │                        ├─┐
        │                        ┌─┘
        │                        │
        └─┐  ┐  ┌───────┬──┐  ┌──┘
          │ ─┤ ─┤       │ ─┤ ─┤
          └──┴──┘       └──┴──┘
              神兽保佑
              代码无BUG!`)

    console.log(`
    .............................................
    佛祖保佑             永无BUG
    佛曰:
             写字楼里写字间，写字间里程序员；
             程序人员写程序，又拿程序换酒钱。
             酒醒只在网上坐，酒醉还来网下眠；
             酒醉酒醒日复日，网上网下年复年。
             但愿老死电脑间，不愿鞠躬老板前；
             奔驰宝马贵者趣，公交自行程序员。
             别人笑我忒疯癫，我笑自己命太贱；
             不见满街漂亮妹，哪个归得程序员？
    `)
    console.log(`
     _______  __   __  _______  __    _  ___   ______    _______  _______  ___     
    |   _   ||  | |  ||       ||  |  | ||   | |    _ |  |       ||       ||   |    
    |  |_|  ||  |_|  ||    ___||   |_| ||   | |   | ||  |  _____||   _   ||   |    
    |       ||       ||   |___ |       ||   | |   |_||_ | |_____ |  | |  ||   |    
    |       ||       ||    ___||  _    ||   | |    __  ||_____  ||  |_|  ||   |___ 
    |   _   | |     | |   |___ | | |   ||   | |   |  | | _____| ||      | |       |
    |__| |__|  |___|  |_______||_|  |__||___| |___|  |_||_______||____||_||_______|
  `);
}

function godBless() {
    //                              _ooOoo_
    //                             o8888888o
    //                             88" . "88
    //                             (| -_- |)
    //                              O\ = /O
    //                           ____/`---'\____
    //                        .   ' \\| |// `.
    //                         / \\||| : |||// \
    //                        / _||||| -:- |||||- \
    //                         | | \\\ - /// | |
    //                       | \_| ''\---/'' | |
    //                        \ .-\__ `-` ___/-. /
    //                    ___`. .' /--.--\ `. . __
    //                  ."" '< `.___\_<|>_/___.' >'"".
    //                 | | : `- \`.;`\ _ /`;.`/ - ` : | |
    //                    \ \ `-. \_ __\ /__ _/ .-` / /
    //           ======`-.____`-.___\_____/___.-`____.-'======
    //                              `=---='
    //
    //           .............................................
    //                     佛祖保佑             永无BUG
    //            佛曰:
    //                     写字楼里写字间，写字间里程序员；
    //                     程序人员写程序，又拿程序换酒钱。
    //                     酒醒只在网上坐，酒醉还来网下眠；
    //                     酒醉酒醒日复日，网上网下年复年。
    //                     但愿老死电脑间，不愿鞠躬老板前；
    //                     奔驰宝马贵者趣，公交自行程序员。
    //                     别人笑我忒疯癫，我笑自己命太贱；
    //                     不见满街漂亮妹，哪个归得程序员？
}



function mythicalAnimalBless() {
    // code is far away from bug with the animal protecting
    //                   ___====-_  _-====___
    //             _--^^^#####//      \\#####^^^--_
    //          _-^##########// (    ) \\##########^-_
    //         -############//  |\^^/|  \\############-
    //       _/############//   (@::@)   \\############\_
    //      /#############((     \\//     ))#############\
    //     -###############\\    (oo)    //###############-
    //    -#################\\  / VV \  //#################-
    //   -###################\\/      \//###################-
    //  _#/|##########/\######(   /\   )######/\##########|\#_
    //  |/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
    //  `  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
    //     `   `  `      `   / | |  | | \   '      '  '   '
    //                      (  | |  | |  )
    //                     __\ | |  | | /__
    //                    (vvv(VVV)(VVV)vvv)
    //                   神兽保佑
    //                 代码无BUG!
    //                                                     __----~~~~~~~~~~~------___
    //                                    .  .   ~~//====......          __--~ ~~
    //                    -.            \_|//     |||\\  ~~~~~~::::... /~
    //                 ___-==_       _-~o~  \/    |||  \\            _/~~-
    //         __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
    //     _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
    //   .~       .~       |   \\ -_    /  /-   /   ||      \   /
    //  /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
    //  |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
    //           '         ~-|      /|    |-~\~~       __--~~
    //                       |-~~-_/ |    |   ~\_   _-~            /\
    //                            /  \     \__   \/~                \__
    //                        _--~ _/ | .-~~____--~-/                  ~~==.
    //                       ((->/~   '.|||' -_|    ~~-/ ,              . _||
    //                                  -_     ~\      ~~---l__i__i__i--~~_/
    //                                  _-~-__   ~)  \--______________--~~
    //                                //.-~~~-~_--~- |-------~~~~~~~~
    //                                       //.-~~~--\
    //                   神兽保佑
    //                 代码无BUG!
}

main();
