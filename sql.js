//主要用来测试sql的解析结果
const sqlparse = require('node-sqlparser');
const readline = require('readline-sync');

const sql = readline.question('sql = ');
const parser = require('js-sql-parser');
var parse = require('node-sqlparser').parse;
var astObj = parse(sql);
console.log("astObj = ", astObj);

console.log("columns = ", astObj.columns);

console.log("sql string = ", JSON.stringify(astObj));



if (astObj.columns && astObj.columns[0].type) {
    console.log("astObj.columns.type = ", astObj.columns[0].type);
}

if (astObj.columns && astObj.columns[0].ext) {
    console.log('astObj.columns.ext = ', astObj.columns[0].ext);
}

if (astObj.values && astObj.values[0].value) {
    console.log('astObj.values[0].value = ', astObj.values[0].value);
}

if (astObj.where && astObj.where.left) {
    console.log("astObj.where.left = ", astObj.where.left);
}


if (astObj.orderby) {
    console.log("orderby = ", astObj.orderby);
}