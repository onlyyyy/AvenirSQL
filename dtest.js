// 本程序用来测试AvenirSQL的通信和工作状态
// 之后可以转为AvenirSQL的专用连接程序
const net = require('net')
const ini = require('ini');
const a = Math.random(1).toString().slice(3, 8);
const b = Math.random(3).toString().slice(3, 8);
const c = Math.random(5).toString().slice(3, 8);
const d = Math.random(7).toString().slice(3, 8);
const e = Math.random(9).toString().slice(3, 8);

const test = {
    //默认
    init: {
        sign: "test",
        type: 'sql',
        data: 'select * from test',
    },
    //登录获得签名
    login: {
        user: 'test',
        password: "123456",
        type: 'login',
    },
    //建库
    createDB: {
        sign: "test",
        type: "sql",
        data: "create database test",
    },
    //测sql为空
    noSql: {
        sign: "test",
        type: 'sql',
        data: '',
    },
    //切换数据库
    useDB: {
        sign: "test",
        type: "sql",
        data: "use test",
    },
    //测sql解析报错
    unknownCmd: {
        sign: "test",
        type: "sql",
        data: "xxxx",
    },
    //测建表
    createTable: {
        sign: "test",
        type: "sql",
        data: `create table t (a number primary key not null index,b string(10) not null,c number(10) not 3,d number ,e number)`,
    },
    //建立User数据库
    DBUser: {
        sign: "test",
        type: "sql",
        data: "create database User",
    },
    //插入数据
    insert: {
        sign: "test",
        type: "sql",
        data: `insert into t(a,b,c,d,e) values(${parseInt(a)},'${b}',${parseInt(c)},${parseInt(d)},${parseInt(e)})`,
    },

    //deletex 和insertx 一起测试用
    delete1: {
        sign: "test",
        type: "sql",
        data: `delete from t where a = '1'`,
    },
    insert1: {
        sign: "test",
        type: "sql",
        data: `insert into t(a,b,c,d) values (1,'2',4,4)`,
    },
    delete2: {
        sign: "test",
        type: "sql",
        data: `delete from t where a = '1' and b = '0'`,
    },
    delete3: {
        sign: "test",
        type: "sql",
        data: `delete from t where b = '3'`,
    },
    insert2: {
        sign: "test",
        type: "sql",
        data: `insert into t(a,b,c,d) values (1,0,6,4)`,
    },
    insert3: {
        sign: "test",
        type: "sql",
        data: `insert into t(a,b,c,d) values ('${a}',3,5,4)`,
    },
    deleteAll: {
        sign: "test",
        type: "sql",
        data: `delete from t`,
    },
    deleteBig: {
        sign: "test",
        type: "sql",
        data: `delete from t where a > '40000'`,
    },
    deleteLow: {
        sign: "test",
        type: "sql",
        data: `delete from t where a < '50000'`,
    },
    deleteNotNum: {
        sign: "test",
        type: "sql",
        data: `delete from t where a < 'a'`,
    },
    deleteMut: {
        sign: "test",
        type: "sql",
        data: `delete from t where a = '1' and b > '10' and c > '10' and d > '10' and e > '10'`,
    },
    insertMut: {
        sign: "test",
        type: "sql",
        data: `insert into t(a,b,c,d,e) values ('1','${b}',5,4,'${e}')`,
    },
    //最复杂 功能测的最全的delete语句
    deleteFinal: {
        sign: "test",
        type: "sql",
        data: `delete from t where a > '${a}' and b > '${b}' and c > '${c}' and d > '${d}' and e < '${e}'`,
    },
    insertText: {
        sign: "test",
        type: "sql",
        data: `insert into t(a,b,c,d,e) values ('测试','今天','天气','不错','${e}')`,
    },
    //测更新
    updateMut: {
        sign: "test",
        type: "sql",
        data: `update t set b = 9999 where a = 18555 and c < 60000 and d < 50000`,
    },
    //20210205 update不带where已经被禁止了
    update1: {
        sign: "test",
        type: "sql",
        data: `update t set c = '10000' where a > 10000`,
    },
    updateKeyFail: {
        sign: "test",
        type: "sql",
        data: `update t set a = '28641' where a = '93937'`,
    },
    updateKey: {
        sign: "test",
        type: "sql",
        data: `update t set a = '${a}' and b = ${b} and c = ${c} where a = '95689' `,
    },
    update: {
        sign: "test",
        type: "sql",
        data: `update t set b = '${b}' and c = ${c} and d = ${d} where b > '40000' and c > '20000'`,
    },
    select: {
        sign: "test",
        type: "sql",
        data: `select a as Avenir,b,c,d,e from t where a > 30000 and b < 80000 and c < 90000 and d < 20000 and e < 90000 `,
    },
    selectRepeat: {
        sign: "test",
        type: "sql",
        data: `select a,b,c,d,e from t where a > 30000 and a < 80000 and c < 90000 and d < 20000 and e < 90000 `,
    },
    selectKey: {
        sign: "test",
        type: "sql",
        data: `select a,b,c,d,e from t where a = 1 and b < 80000  `,
    },
    selectAll: {
        sign: "test",
        type: "sql",
        data: `select a,b,c,d,e from t where a >10000 and b >10000  and c >20000 and d > 10000 and e < 90000 `,
    },
    selectCount: {
        sign: "test",
        type: "sql",
        data: `select count(*) as aaa from t where a >10000 and b >10000  and c >20000 and d > 10000 and e < 90000 `,
    },
    selectFail: {
        sign: "test",
        type: "sql",
        data: `select a,c from t where a > 10000`,
    },
    selectOrder: {
        sign: "test",
        type: "sql",
        data: `select a,c from t where a > 10000 order by a asc , b desc limit 10`,
    },
    dropDB: {
        sign: "test",
        type: "sql",
        data: `drop database a`,
    },
    createA: {
        sign: "test",
        type: "sql",
        data: "create database a",
    },
    dropTable: {
        sign: "test",
        type: "sql",
        data: `drop table a`,
    },
    createTA: {
        sign: "test",
        type: "sql",
        data: `create table a (id int primary key , name varchar  , num int not null)`,
    },
    createTUser: {
        sign: "test",
        type: "sql",
        data: `create table User.user (name varchar primary key ,password varchar not null , auth varchar not null)`,
    },
    dropTUser: {
        sign: "test",
        type: "sql",
        data: `drop table User.user`,
    },
    //事务部分
    begin: {
        sign: "test",
        type: "trans",
        data: `begin`,
    },
    transSelect: {
        sign: "test",
        id: "test",
        type: "trans",
        data: "select * from t",
    },
    transInsert: {
        sign: "test",
        id: "test",
        type: "trans",
        data: `insert into t(a,b,c,d,e) values('${a}','${b}','${c}','${d}','${e}')`,
    },
    transUpdate: {
        sign: "test",
        id: "test",
        type: "trans",
        data: "update t set a = 10 where  a > 10000",
    },
    transDelete: {
        sign: "test",
        id: "test",
        type: "trans",
        data: "delete from t where a > 40000",
    },
    transError: {
        sign: "test",
        id: "test",
        type: "trans",
        data: "delete from t  a = 10",
    },
    //提交
    commit: {
        sign: "test",
        id: "test",
        type: "trans",
        data: "commit",
    },
    rollback: {
        sign: "test",
        id: "test",
        type: "trans",
        data: "rollback",
    },
    distinct: {
        sign: "test",
        id: "test",
        type: "sql",
        data: "select distinct a,b,c from t",
    },
    dump: {
        sign: "test",
        id: "test",
        type: "sql",
        data: "dump table t as select * from t",
    },
    showDump:{
        sign: "test",
        id: "test",
        type: "sql",
        data: "show dump",
    }
}


//要发送给服务器的数据
let send = {};

async function main() {
    let type = process.argv.splice(2);
    if (type[0] === '-help' || type[0] === '--help' || type[0] === '-h') {
        console.log(test);
        process.exit(0);
    }
    if (type.length == 0) {
        type[0] = 'init';
    }
    if (type[0] != '-s' || type[0] == '--sql') {
        send = test[type[0]];

    } else {
        send = {
            sign: "test",
            type: "sql",
            data: type[1],
        }
    }
    let client = new net.Socket();
    client.connect({
        host: "127.0.0.1",
        port: 44944
    })

    //客户端与服务器建立连接触发
    client.on('connect', () => {
        client.write(JSON.stringify(send));
    });

    //客户端接收数据触发
    client.on('data', function (data) {
        console.log('服务器发送的数据 : ', data.toString());
        client.end();
    });

}

main();